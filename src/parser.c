#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 293
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 111
#define ALIAS_COUNT 0
#define TOKEN_COUNT 60
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 15
#define MAX_ALIAS_SEQUENCE_LENGTH 11
#define PRODUCTION_ID_COUNT 29

enum {
  anon_sym_POUND = 1,
  anon_sym_LBRACK = 2,
  anon_sym_RBRACK = 3,
  anon_sym_module = 4,
  anon_sym_SEMI = 5,
  anon_sym_COLON_COLON = 6,
  anon_sym_entity = 7,
  anon_sym_LPAREN = 8,
  anon_sym_RPAREN = 9,
  anon_sym_concept = 10,
  anon_sym_COLON = 11,
  anon_sym_DASH_GT = 12,
  anon_sym_operation = 13,
  anon_sym_impl = 14,
  anon_sym_LT = 15,
  anon_sym_GT = 16,
  anon_sym_for = 17,
  anon_sym_export = 18,
  anon_sym_fn = 19,
  anon_sym_COMMA = 20,
  anon_sym_LBRACE = 21,
  anon_sym_RBRACE = 22,
  anon_sym_let = 23,
  anon_sym_EQ = 24,
  anon_sym_csg = 25,
  anon_sym_if = 26,
  anon_sym_else = 27,
  anon_sym_in = 28,
  anon_sym_DOT_DOT = 29,
  anon_sym_eval = 30,
  anon_sym_DOT = 31,
  anon_sym_DASH = 32,
  anon_sym_BANG = 33,
  anon_sym_SLASH = 34,
  anon_sym_STAR = 35,
  anon_sym_PLUS = 36,
  anon_sym_PERCENT = 37,
  anon_sym_EQ_EQ = 38,
  anon_sym_BANG_EQ = 39,
  anon_sym_LT_EQ = 40,
  anon_sym_GT_EQ = 41,
  anon_sym_AMP_AMP = 42,
  anon_sym_PIPE_PIPE = 43,
  anon_sym_interval = 44,
  anon_sym_vec = 45,
  anon_sym_mat = 46,
  anon_sym_x = 47,
  anon_sym_tensor = 48,
  anon_sym_real = 49,
  anon_sym_int = 50,
  anon_sym_complex = 51,
  anon_sym_quat = 52,
  anon_sym_bool = 53,
  anon_sym_none = 54,
  anon_sym_true = 55,
  anon_sym_false = 56,
  sym_digit = 57,
  sym_identifier = 58,
  sym_comment = 59,
  sym_source_file = 60,
  sym_ct_attrib = 61,
  sym_module = 62,
  sym__path = 63,
  sym_def_entity = 64,
  sym_def_concept = 65,
  sym_def_operation = 66,
  sym_impl_block = 67,
  sym_fn = 68,
  sym__arg_list = 69,
  sym_typed_arg = 70,
  sym__ident_list = 71,
  sym_block = 72,
  sym_let = 73,
  sym_assign = 74,
  sym_csg = 75,
  sym_stmt = 76,
  sym_expr = 77,
  sym_gamma_expr = 78,
  sym_theta_expr = 79,
  sym_eval_expr = 80,
  sym_unary_expr = 81,
  sym_field_access = 82,
  sym__field_accessor = 83,
  sym_splat_expr = 84,
  sym_binary_expr = 85,
  sym_fn_call = 86,
  sym_scope_call = 87,
  sym__param_list = 88,
  sym_type = 89,
  sym_shape = 90,
  sym__shaped_type = 91,
  sym_data_type = 92,
  sym_list = 93,
  sym_tuple = 94,
  sym__literal = 95,
  sym_float_literal = 96,
  sym_bool_literal = 97,
  sym_int_literal = 98,
  aux_sym_source_file_repeat1 = 99,
  aux_sym__path_repeat1 = 100,
  aux_sym__arg_list_repeat1 = 101,
  aux_sym__ident_list_repeat1 = 102,
  aux_sym_block_repeat1 = 103,
  aux_sym_block_repeat2 = 104,
  aux_sym_field_access_repeat1 = 105,
  aux_sym_scope_call_repeat1 = 106,
  aux_sym__param_list_repeat1 = 107,
  aux_sym_type_repeat1 = 108,
  aux_sym_shape_repeat1 = 109,
  aux_sym_list_repeat1 = 110,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [anon_sym_POUND] = "#",
  [anon_sym_LBRACK] = "[",
  [anon_sym_RBRACK] = "]",
  [anon_sym_module] = "module",
  [anon_sym_SEMI] = ";",
  [anon_sym_COLON_COLON] = "::",
  [anon_sym_entity] = "entity",
  [anon_sym_LPAREN] = "(",
  [anon_sym_RPAREN] = ")",
  [anon_sym_concept] = "concept",
  [anon_sym_COLON] = ":",
  [anon_sym_DASH_GT] = "->",
  [anon_sym_operation] = "operation",
  [anon_sym_impl] = "impl",
  [anon_sym_LT] = "<",
  [anon_sym_GT] = ">",
  [anon_sym_for] = "for",
  [anon_sym_export] = "export",
  [anon_sym_fn] = "fn",
  [anon_sym_COMMA] = ",",
  [anon_sym_LBRACE] = "{",
  [anon_sym_RBRACE] = "}",
  [anon_sym_let] = "let",
  [anon_sym_EQ] = "=",
  [anon_sym_csg] = "csg",
  [anon_sym_if] = "if",
  [anon_sym_else] = "else",
  [anon_sym_in] = "in",
  [anon_sym_DOT_DOT] = "..",
  [anon_sym_eval] = "eval",
  [anon_sym_DOT] = ".",
  [anon_sym_DASH] = "-",
  [anon_sym_BANG] = "!",
  [anon_sym_SLASH] = "/",
  [anon_sym_STAR] = "*",
  [anon_sym_PLUS] = "+",
  [anon_sym_PERCENT] = "%",
  [anon_sym_EQ_EQ] = "==",
  [anon_sym_BANG_EQ] = "!=",
  [anon_sym_LT_EQ] = "<=",
  [anon_sym_GT_EQ] = ">=",
  [anon_sym_AMP_AMP] = "&&",
  [anon_sym_PIPE_PIPE] = "||",
  [anon_sym_interval] = "interval",
  [anon_sym_vec] = "vec",
  [anon_sym_mat] = "mat",
  [anon_sym_x] = "x",
  [anon_sym_tensor] = "tensor",
  [anon_sym_real] = "real",
  [anon_sym_int] = "int",
  [anon_sym_complex] = "complex",
  [anon_sym_quat] = "quat",
  [anon_sym_bool] = "bool",
  [anon_sym_none] = "none",
  [anon_sym_true] = "true",
  [anon_sym_false] = "false",
  [sym_digit] = "digit",
  [sym_identifier] = "identifier",
  [sym_comment] = "comment",
  [sym_source_file] = "source_file",
  [sym_ct_attrib] = "ct_attrib",
  [sym_module] = "module",
  [sym__path] = "_path",
  [sym_def_entity] = "def_entity",
  [sym_def_concept] = "def_concept",
  [sym_def_operation] = "def_operation",
  [sym_impl_block] = "impl_block",
  [sym_fn] = "fn",
  [sym__arg_list] = "_arg_list",
  [sym_typed_arg] = "typed_arg",
  [sym__ident_list] = "_ident_list",
  [sym_block] = "block",
  [sym_let] = "let",
  [sym_assign] = "assign",
  [sym_csg] = "csg",
  [sym_stmt] = "stmt",
  [sym_expr] = "expr",
  [sym_gamma_expr] = "gamma_expr",
  [sym_theta_expr] = "theta_expr",
  [sym_eval_expr] = "eval_expr",
  [sym_unary_expr] = "unary_expr",
  [sym_field_access] = "field_access",
  [sym__field_accessor] = "_field_accessor",
  [sym_splat_expr] = "splat_expr",
  [sym_binary_expr] = "binary_expr",
  [sym_fn_call] = "fn_call",
  [sym_scope_call] = "scope_call",
  [sym__param_list] = "_param_list",
  [sym_type] = "type",
  [sym_shape] = "shape",
  [sym__shaped_type] = "_shaped_type",
  [sym_data_type] = "data_type",
  [sym_list] = "list",
  [sym_tuple] = "tuple",
  [sym__literal] = "_literal",
  [sym_float_literal] = "float_literal",
  [sym_bool_literal] = "bool_literal",
  [sym_int_literal] = "int_literal",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
  [aux_sym__path_repeat1] = "_path_repeat1",
  [aux_sym__arg_list_repeat1] = "_arg_list_repeat1",
  [aux_sym__ident_list_repeat1] = "_ident_list_repeat1",
  [aux_sym_block_repeat1] = "block_repeat1",
  [aux_sym_block_repeat2] = "block_repeat2",
  [aux_sym_field_access_repeat1] = "field_access_repeat1",
  [aux_sym_scope_call_repeat1] = "scope_call_repeat1",
  [aux_sym__param_list_repeat1] = "_param_list_repeat1",
  [aux_sym_type_repeat1] = "type_repeat1",
  [aux_sym_shape_repeat1] = "shape_repeat1",
  [aux_sym_list_repeat1] = "list_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [anon_sym_POUND] = anon_sym_POUND,
  [anon_sym_LBRACK] = anon_sym_LBRACK,
  [anon_sym_RBRACK] = anon_sym_RBRACK,
  [anon_sym_module] = anon_sym_module,
  [anon_sym_SEMI] = anon_sym_SEMI,
  [anon_sym_COLON_COLON] = anon_sym_COLON_COLON,
  [anon_sym_entity] = anon_sym_entity,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [anon_sym_concept] = anon_sym_concept,
  [anon_sym_COLON] = anon_sym_COLON,
  [anon_sym_DASH_GT] = anon_sym_DASH_GT,
  [anon_sym_operation] = anon_sym_operation,
  [anon_sym_impl] = anon_sym_impl,
  [anon_sym_LT] = anon_sym_LT,
  [anon_sym_GT] = anon_sym_GT,
  [anon_sym_for] = anon_sym_for,
  [anon_sym_export] = anon_sym_export,
  [anon_sym_fn] = anon_sym_fn,
  [anon_sym_COMMA] = anon_sym_COMMA,
  [anon_sym_LBRACE] = anon_sym_LBRACE,
  [anon_sym_RBRACE] = anon_sym_RBRACE,
  [anon_sym_let] = anon_sym_let,
  [anon_sym_EQ] = anon_sym_EQ,
  [anon_sym_csg] = anon_sym_csg,
  [anon_sym_if] = anon_sym_if,
  [anon_sym_else] = anon_sym_else,
  [anon_sym_in] = anon_sym_in,
  [anon_sym_DOT_DOT] = anon_sym_DOT_DOT,
  [anon_sym_eval] = anon_sym_eval,
  [anon_sym_DOT] = anon_sym_DOT,
  [anon_sym_DASH] = anon_sym_DASH,
  [anon_sym_BANG] = anon_sym_BANG,
  [anon_sym_SLASH] = anon_sym_SLASH,
  [anon_sym_STAR] = anon_sym_STAR,
  [anon_sym_PLUS] = anon_sym_PLUS,
  [anon_sym_PERCENT] = anon_sym_PERCENT,
  [anon_sym_EQ_EQ] = anon_sym_EQ_EQ,
  [anon_sym_BANG_EQ] = anon_sym_BANG_EQ,
  [anon_sym_LT_EQ] = anon_sym_LT_EQ,
  [anon_sym_GT_EQ] = anon_sym_GT_EQ,
  [anon_sym_AMP_AMP] = anon_sym_AMP_AMP,
  [anon_sym_PIPE_PIPE] = anon_sym_PIPE_PIPE,
  [anon_sym_interval] = anon_sym_interval,
  [anon_sym_vec] = anon_sym_vec,
  [anon_sym_mat] = anon_sym_mat,
  [anon_sym_x] = anon_sym_x,
  [anon_sym_tensor] = anon_sym_tensor,
  [anon_sym_real] = anon_sym_real,
  [anon_sym_int] = anon_sym_int,
  [anon_sym_complex] = anon_sym_complex,
  [anon_sym_quat] = anon_sym_quat,
  [anon_sym_bool] = anon_sym_bool,
  [anon_sym_none] = anon_sym_none,
  [anon_sym_true] = anon_sym_true,
  [anon_sym_false] = anon_sym_false,
  [sym_digit] = sym_digit,
  [sym_identifier] = sym_identifier,
  [sym_comment] = sym_comment,
  [sym_source_file] = sym_source_file,
  [sym_ct_attrib] = sym_ct_attrib,
  [sym_module] = sym_module,
  [sym__path] = sym__path,
  [sym_def_entity] = sym_def_entity,
  [sym_def_concept] = sym_def_concept,
  [sym_def_operation] = sym_def_operation,
  [sym_impl_block] = sym_impl_block,
  [sym_fn] = sym_fn,
  [sym__arg_list] = sym__arg_list,
  [sym_typed_arg] = sym_typed_arg,
  [sym__ident_list] = sym__ident_list,
  [sym_block] = sym_block,
  [sym_let] = sym_let,
  [sym_assign] = sym_assign,
  [sym_csg] = sym_csg,
  [sym_stmt] = sym_stmt,
  [sym_expr] = sym_expr,
  [sym_gamma_expr] = sym_gamma_expr,
  [sym_theta_expr] = sym_theta_expr,
  [sym_eval_expr] = sym_eval_expr,
  [sym_unary_expr] = sym_unary_expr,
  [sym_field_access] = sym_field_access,
  [sym__field_accessor] = sym__field_accessor,
  [sym_splat_expr] = sym_splat_expr,
  [sym_binary_expr] = sym_binary_expr,
  [sym_fn_call] = sym_fn_call,
  [sym_scope_call] = sym_scope_call,
  [sym__param_list] = sym__param_list,
  [sym_type] = sym_type,
  [sym_shape] = sym_shape,
  [sym__shaped_type] = sym__shaped_type,
  [sym_data_type] = sym_data_type,
  [sym_list] = sym_list,
  [sym_tuple] = sym_tuple,
  [sym__literal] = sym__literal,
  [sym_float_literal] = sym_float_literal,
  [sym_bool_literal] = sym_bool_literal,
  [sym_int_literal] = sym_int_literal,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
  [aux_sym__path_repeat1] = aux_sym__path_repeat1,
  [aux_sym__arg_list_repeat1] = aux_sym__arg_list_repeat1,
  [aux_sym__ident_list_repeat1] = aux_sym__ident_list_repeat1,
  [aux_sym_block_repeat1] = aux_sym_block_repeat1,
  [aux_sym_block_repeat2] = aux_sym_block_repeat2,
  [aux_sym_field_access_repeat1] = aux_sym_field_access_repeat1,
  [aux_sym_scope_call_repeat1] = aux_sym_scope_call_repeat1,
  [aux_sym__param_list_repeat1] = aux_sym__param_list_repeat1,
  [aux_sym_type_repeat1] = aux_sym_type_repeat1,
  [aux_sym_shape_repeat1] = aux_sym_shape_repeat1,
  [aux_sym_list_repeat1] = aux_sym_list_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_POUND] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_module] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SEMI] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COLON_COLON] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_entity] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_concept] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COLON] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DASH_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_operation] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_impl] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_for] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_export] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_fn] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COMMA] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_let] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_csg] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_if] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_else] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_in] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_eval] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_BANG] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SLASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_STAR] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PLUS] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PERCENT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_BANG_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_AMP_AMP] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PIPE_PIPE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_interval] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_vec] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_mat] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_x] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_tensor] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_real] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_int] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_complex] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_quat] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_bool] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_none] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_true] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_false] = {
    .visible = true,
    .named = false,
  },
  [sym_digit] = {
    .visible = true,
    .named = true,
  },
  [sym_identifier] = {
    .visible = true,
    .named = true,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym_ct_attrib] = {
    .visible = true,
    .named = true,
  },
  [sym_module] = {
    .visible = true,
    .named = true,
  },
  [sym__path] = {
    .visible = false,
    .named = true,
  },
  [sym_def_entity] = {
    .visible = true,
    .named = true,
  },
  [sym_def_concept] = {
    .visible = true,
    .named = true,
  },
  [sym_def_operation] = {
    .visible = true,
    .named = true,
  },
  [sym_impl_block] = {
    .visible = true,
    .named = true,
  },
  [sym_fn] = {
    .visible = true,
    .named = true,
  },
  [sym__arg_list] = {
    .visible = false,
    .named = true,
  },
  [sym_typed_arg] = {
    .visible = true,
    .named = true,
  },
  [sym__ident_list] = {
    .visible = false,
    .named = true,
  },
  [sym_block] = {
    .visible = true,
    .named = true,
  },
  [sym_let] = {
    .visible = true,
    .named = true,
  },
  [sym_assign] = {
    .visible = true,
    .named = true,
  },
  [sym_csg] = {
    .visible = true,
    .named = true,
  },
  [sym_stmt] = {
    .visible = true,
    .named = true,
  },
  [sym_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_gamma_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_theta_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_eval_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_unary_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_field_access] = {
    .visible = true,
    .named = true,
  },
  [sym__field_accessor] = {
    .visible = false,
    .named = true,
  },
  [sym_splat_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_binary_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_fn_call] = {
    .visible = true,
    .named = true,
  },
  [sym_scope_call] = {
    .visible = true,
    .named = true,
  },
  [sym__param_list] = {
    .visible = false,
    .named = true,
  },
  [sym_type] = {
    .visible = true,
    .named = true,
  },
  [sym_shape] = {
    .visible = true,
    .named = true,
  },
  [sym__shaped_type] = {
    .visible = false,
    .named = true,
  },
  [sym_data_type] = {
    .visible = true,
    .named = true,
  },
  [sym_list] = {
    .visible = true,
    .named = true,
  },
  [sym_tuple] = {
    .visible = true,
    .named = true,
  },
  [sym__literal] = {
    .visible = false,
    .named = true,
  },
  [sym_float_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_bool_literal] = {
    .visible = true,
    .named = true,
  },
  [sym_int_literal] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__path_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__arg_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__ident_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_block_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_block_repeat2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_field_access_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_scope_call_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym__param_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_type_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_shape_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_list_repeat1] = {
    .visible = false,
    .named = false,
  },
};

enum {
  field_access_field = 1,
  field_access_splitter = 2,
  field_concept = 3,
  field_concept_name = 4,
  field_concept_result = 5,
  field_dtype = 6,
  field_entity_name = 7,
  field_fnname = 8,
  field_function_name = 9,
  field_op_or_entity = 10,
  field_operand = 11,
  field_operation_name = 12,
  field_parameter = 13,
  field_path_spacer = 14,
  field_shape = 15,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_access_field] = "access_field",
  [field_access_splitter] = "access_splitter",
  [field_concept] = "concept",
  [field_concept_name] = "concept_name",
  [field_concept_result] = "concept_result",
  [field_dtype] = "dtype",
  [field_entity_name] = "entity_name",
  [field_fnname] = "fnname",
  [field_function_name] = "function_name",
  [field_op_or_entity] = "op_or_entity",
  [field_operand] = "operand",
  [field_operation_name] = "operation_name",
  [field_parameter] = "parameter",
  [field_path_spacer] = "path_spacer",
  [field_shape] = "shape",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [1] = {.index = 0, .length = 1},
  [2] = {.index = 1, .length = 1},
  [3] = {.index = 2, .length = 1},
  [4] = {.index = 3, .length = 2},
  [5] = {.index = 5, .length = 1},
  [6] = {.index = 6, .length = 2},
  [7] = {.index = 8, .length = 1},
  [8] = {.index = 9, .length = 1},
  [9] = {.index = 10, .length = 1},
  [10] = {.index = 11, .length = 1},
  [11] = {.index = 12, .length = 1},
  [12] = {.index = 13, .length = 2},
  [13] = {.index = 15, .length = 2},
  [14] = {.index = 17, .length = 2},
  [15] = {.index = 19, .length = 1},
  [16] = {.index = 20, .length = 1},
  [17] = {.index = 21, .length = 2},
  [18] = {.index = 23, .length = 2},
  [19] = {.index = 25, .length = 4},
  [20] = {.index = 29, .length = 4},
  [21] = {.index = 33, .length = 2},
  [22] = {.index = 35, .length = 2},
  [23] = {.index = 37, .length = 2},
  [24] = {.index = 39, .length = 1},
  [25] = {.index = 40, .length = 2},
  [26] = {.index = 42, .length = 1},
  [27] = {.index = 43, .length = 3},
  [28] = {.index = 46, .length = 1},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_path_spacer, 1},
  [1] =
    {field_path_spacer, 1, .inherited = true},
  [2] =
    {field_path_spacer, 0, .inherited = true},
  [3] =
    {field_path_spacer, 0, .inherited = true},
    {field_path_spacer, 1, .inherited = true},
  [5] =
    {field_shape, 0},
  [6] =
    {field_dtype, 0, .inherited = true},
    {field_shape, 0, .inherited = true},
  [8] =
    {field_dtype, 0},
  [9] =
    {field_function_name, 0},
  [10] =
    {field_parameter, 0},
  [11] =
    {field_entity_name, 1},
  [12] =
    {field_operation_name, 1},
  [13] =
    {field_function_name, 0},
    {field_parameter, 2, .inherited = true},
  [15] =
    {field_parameter, 0, .inherited = true},
    {field_parameter, 1},
  [17] =
    {field_parameter, 0, .inherited = true},
    {field_parameter, 1, .inherited = true},
  [19] =
    {field_access_splitter, 0},
  [20] =
    {field_access_field, 2},
  [21] =
    {field_access_splitter, 0, .inherited = true},
    {field_access_splitter, 1, .inherited = true},
  [23] =
    {field_dtype, 1, .inherited = true},
    {field_shape, 1, .inherited = true},
  [25] =
    {field_dtype, 1, .inherited = true},
    {field_dtype, 2, .inherited = true},
    {field_shape, 1, .inherited = true},
    {field_shape, 2, .inherited = true},
  [29] =
    {field_dtype, 0, .inherited = true},
    {field_dtype, 1, .inherited = true},
    {field_shape, 0, .inherited = true},
    {field_shape, 1, .inherited = true},
  [33] =
    {field_concept_name, 1},
    {field_concept_result, 5},
  [35] =
    {field_dtype, 2},
    {field_shape, 0},
  [37] =
    {field_concept, 3},
    {field_op_or_entity, 1},
  [39] =
    {field_fnname, 1},
  [40] =
    {field_access_field, 3},
    {field_access_splitter, 1, .inherited = true},
  [42] =
    {field_fnname, 2},
  [43] =
    {field_concept, 6},
    {field_op_or_entity, 1},
    {field_operand, 3},
  [46] =
    {field_parameter, 5, .inherited = true},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 2,
  [5] = 3,
  [6] = 2,
  [7] = 3,
  [8] = 8,
  [9] = 9,
  [10] = 10,
  [11] = 11,
  [12] = 12,
  [13] = 13,
  [14] = 14,
  [15] = 11,
  [16] = 12,
  [17] = 9,
  [18] = 10,
  [19] = 19,
  [20] = 20,
  [21] = 11,
  [22] = 12,
  [23] = 9,
  [24] = 10,
  [25] = 25,
  [26] = 26,
  [27] = 27,
  [28] = 28,
  [29] = 29,
  [30] = 30,
  [31] = 31,
  [32] = 32,
  [33] = 32,
  [34] = 34,
  [35] = 32,
  [36] = 36,
  [37] = 37,
  [38] = 38,
  [39] = 39,
  [40] = 40,
  [41] = 37,
  [42] = 42,
  [43] = 34,
  [44] = 44,
  [45] = 45,
  [46] = 38,
  [47] = 47,
  [48] = 48,
  [49] = 45,
  [50] = 42,
  [51] = 36,
  [52] = 34,
  [53] = 37,
  [54] = 36,
  [55] = 42,
  [56] = 38,
  [57] = 57,
  [58] = 58,
  [59] = 59,
  [60] = 60,
  [61] = 61,
  [62] = 62,
  [63] = 14,
  [64] = 64,
  [65] = 65,
  [66] = 66,
  [67] = 67,
  [68] = 68,
  [69] = 69,
  [70] = 70,
  [71] = 71,
  [72] = 72,
  [73] = 73,
  [74] = 74,
  [75] = 75,
  [76] = 76,
  [77] = 77,
  [78] = 78,
  [79] = 79,
  [80] = 80,
  [81] = 81,
  [82] = 82,
  [83] = 83,
  [84] = 84,
  [85] = 85,
  [86] = 86,
  [87] = 87,
  [88] = 59,
  [89] = 89,
  [90] = 60,
  [91] = 91,
  [92] = 89,
  [93] = 93,
  [94] = 94,
  [95] = 89,
  [96] = 96,
  [97] = 94,
  [98] = 98,
  [99] = 99,
  [100] = 100,
  [101] = 101,
  [102] = 94,
  [103] = 103,
  [104] = 58,
  [105] = 105,
  [106] = 76,
  [107] = 107,
  [108] = 71,
  [109] = 109,
  [110] = 110,
  [111] = 111,
  [112] = 112,
  [113] = 113,
  [114] = 72,
  [115] = 115,
  [116] = 112,
  [117] = 117,
  [118] = 118,
  [119] = 119,
  [120] = 82,
  [121] = 121,
  [122] = 122,
  [123] = 81,
  [124] = 124,
  [125] = 125,
  [126] = 126,
  [127] = 85,
  [128] = 83,
  [129] = 81,
  [130] = 85,
  [131] = 82,
  [132] = 132,
  [133] = 83,
  [134] = 134,
  [135] = 135,
  [136] = 136,
  [137] = 137,
  [138] = 138,
  [139] = 139,
  [140] = 140,
  [141] = 141,
  [142] = 142,
  [143] = 143,
  [144] = 144,
  [145] = 145,
  [146] = 146,
  [147] = 147,
  [148] = 148,
  [149] = 149,
  [150] = 150,
  [151] = 151,
  [152] = 152,
  [153] = 153,
  [154] = 154,
  [155] = 155,
  [156] = 156,
  [157] = 157,
  [158] = 158,
  [159] = 159,
  [160] = 160,
  [161] = 161,
  [162] = 162,
  [163] = 163,
  [164] = 164,
  [165] = 165,
  [166] = 166,
  [167] = 167,
  [168] = 168,
  [169] = 169,
  [170] = 170,
  [171] = 171,
  [172] = 172,
  [173] = 173,
  [174] = 174,
  [175] = 175,
  [176] = 170,
  [177] = 177,
  [178] = 178,
  [179] = 179,
  [180] = 177,
  [181] = 181,
  [182] = 181,
  [183] = 183,
  [184] = 179,
  [185] = 185,
  [186] = 186,
  [187] = 179,
  [188] = 188,
  [189] = 177,
  [190] = 190,
  [191] = 191,
  [192] = 192,
  [193] = 193,
  [194] = 194,
  [195] = 195,
  [196] = 196,
  [197] = 197,
  [198] = 198,
  [199] = 199,
  [200] = 200,
  [201] = 201,
  [202] = 202,
  [203] = 203,
  [204] = 204,
  [205] = 205,
  [206] = 206,
  [207] = 207,
  [208] = 208,
  [209] = 209,
  [210] = 210,
  [211] = 211,
  [212] = 205,
  [213] = 213,
  [214] = 214,
  [215] = 215,
  [216] = 216,
  [217] = 217,
  [218] = 218,
  [219] = 202,
  [220] = 220,
  [221] = 221,
  [222] = 222,
  [223] = 223,
  [224] = 224,
  [225] = 225,
  [226] = 226,
  [227] = 227,
  [228] = 228,
  [229] = 229,
  [230] = 230,
  [231] = 231,
  [232] = 232,
  [233] = 233,
  [234] = 234,
  [235] = 235,
  [236] = 236,
  [237] = 237,
  [238] = 238,
  [239] = 239,
  [240] = 240,
  [241] = 241,
  [242] = 242,
  [243] = 243,
  [244] = 244,
  [245] = 245,
  [246] = 246,
  [247] = 247,
  [248] = 248,
  [249] = 249,
  [250] = 250,
  [251] = 251,
  [252] = 252,
  [253] = 253,
  [254] = 254,
  [255] = 255,
  [256] = 256,
  [257] = 257,
  [258] = 258,
  [259] = 259,
  [260] = 260,
  [261] = 261,
  [262] = 262,
  [263] = 263,
  [264] = 264,
  [265] = 265,
  [266] = 266,
  [267] = 267,
  [268] = 268,
  [269] = 269,
  [270] = 270,
  [271] = 271,
  [272] = 272,
  [273] = 273,
  [274] = 274,
  [275] = 275,
  [276] = 276,
  [277] = 277,
  [278] = 278,
  [279] = 279,
  [280] = 280,
  [281] = 281,
  [282] = 282,
  [283] = 283,
  [284] = 284,
  [285] = 285,
  [286] = 286,
  [287] = 287,
  [288] = 288,
  [289] = 289,
  [290] = 290,
  [291] = 291,
  [292] = 292,
};

static inline bool sym_identifier_character_set_1(int32_t c) {
  return (c < 43514
    ? (c < 4193
      ? (c < 2707
        ? (c < 1994
          ? (c < 910
            ? (c < 736
              ? (c < 186
                ? (c < 'a'
                  ? (c < '_'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= '_')
                  : (c <= 'z' || (c < 181
                    ? c == 170
                    : c <= 181)))
                : (c <= 186 || (c < 248
                  ? (c < 216
                    ? (c >= 192 && c <= 214)
                    : c <= 246)
                  : (c <= 705 || (c >= 710 && c <= 721)))))
              : (c <= 740 || (c < 891
                ? (c < 880
                  ? (c < 750
                    ? c == 748
                    : c <= 750)
                  : (c <= 884 || (c >= 886 && c <= 887)))
                : (c <= 893 || (c < 904
                  ? (c < 902
                    ? c == 895
                    : c <= 902)
                  : (c <= 906 || c == 908))))))
            : (c <= 929 || (c < 1649
              ? (c < 1376
                ? (c < 1162
                  ? (c < 1015
                    ? (c >= 931 && c <= 1013)
                    : c <= 1153)
                  : (c <= 1327 || (c < 1369
                    ? (c >= 1329 && c <= 1366)
                    : c <= 1369)))
                : (c <= 1416 || (c < 1568
                  ? (c < 1519
                    ? (c >= 1488 && c <= 1514)
                    : c <= 1522)
                  : (c <= 1610 || (c >= 1646 && c <= 1647)))))
              : (c <= 1747 || (c < 1791
                ? (c < 1774
                  ? (c < 1765
                    ? c == 1749
                    : c <= 1766)
                  : (c <= 1775 || (c >= 1786 && c <= 1788)))
                : (c <= 1791 || (c < 1869
                  ? (c < 1810
                    ? c == 1808
                    : c <= 1839)
                  : (c <= 1957 || c == 1969))))))))
          : (c <= 2026 || (c < 2482
            ? (c < 2208
              ? (c < 2088
                ? (c < 2048
                  ? (c < 2042
                    ? (c >= 2036 && c <= 2037)
                    : c <= 2042)
                  : (c <= 2069 || (c < 2084
                    ? c == 2074
                    : c <= 2084)))
                : (c <= 2088 || (c < 2160
                  ? (c < 2144
                    ? (c >= 2112 && c <= 2136)
                    : c <= 2154)
                  : (c <= 2183 || (c >= 2185 && c <= 2190)))))
              : (c <= 2249 || (c < 2417
                ? (c < 2384
                  ? (c < 2365
                    ? (c >= 2308 && c <= 2361)
                    : c <= 2365)
                  : (c <= 2384 || (c >= 2392 && c <= 2401)))
                : (c <= 2432 || (c < 2451
                  ? (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)
                  : (c <= 2472 || (c >= 2474 && c <= 2480)))))))
            : (c <= 2482 || (c < 2579
              ? (c < 2527
                ? (c < 2510
                  ? (c < 2493
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2493)
                  : (c <= 2510 || (c >= 2524 && c <= 2525)))
                : (c <= 2529 || (c < 2565
                  ? (c < 2556
                    ? (c >= 2544 && c <= 2545)
                    : c <= 2556)
                  : (c <= 2570 || (c >= 2575 && c <= 2576)))))
              : (c <= 2600 || (c < 2649
                ? (c < 2613
                  ? (c < 2610
                    ? (c >= 2602 && c <= 2608)
                    : c <= 2611)
                  : (c <= 2614 || (c >= 2616 && c <= 2617)))
                : (c <= 2652 || (c < 2693
                  ? (c < 2674
                    ? c == 2654
                    : c <= 2676)
                  : (c <= 2701 || (c >= 2703 && c <= 2705)))))))))))
        : (c <= 2728 || (c < 3242
          ? (c < 2962
            ? (c < 2858
              ? (c < 2784
                ? (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2768
                    ? c == 2749
                    : c <= 2768)))
                : (c <= 2785 || (c < 2831
                  ? (c < 2821
                    ? c == 2809
                    : c <= 2828)
                  : (c <= 2832 || (c >= 2835 && c <= 2856)))))
              : (c <= 2864 || (c < 2911
                ? (c < 2877
                  ? (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)
                  : (c <= 2877 || (c >= 2908 && c <= 2909)))
                : (c <= 2913 || (c < 2949
                  ? (c < 2947
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c >= 2958 && c <= 2960)))))))
            : (c <= 2965 || (c < 3090
              ? (c < 2984
                ? (c < 2974
                  ? (c < 2972
                    ? (c >= 2969 && c <= 2970)
                    : c <= 2972)
                  : (c <= 2975 || (c >= 2979 && c <= 2980)))
                : (c <= 2986 || (c < 3077
                  ? (c < 3024
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3024)
                  : (c <= 3084 || (c >= 3086 && c <= 3088)))))
              : (c <= 3112 || (c < 3168
                ? (c < 3160
                  ? (c < 3133
                    ? (c >= 3114 && c <= 3129)
                    : c <= 3133)
                  : (c <= 3162 || c == 3165))
                : (c <= 3169 || (c < 3214
                  ? (c < 3205
                    ? c == 3200
                    : c <= 3212)
                  : (c <= 3216 || (c >= 3218 && c <= 3240)))))))))
          : (c <= 3251 || (c < 3648
            ? (c < 3412
              ? (c < 3332
                ? (c < 3293
                  ? (c < 3261
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3261)
                  : (c <= 3294 || (c < 3313
                    ? (c >= 3296 && c <= 3297)
                    : c <= 3314)))
                : (c <= 3340 || (c < 3389
                  ? (c < 3346
                    ? (c >= 3342 && c <= 3344)
                    : c <= 3386)
                  : (c <= 3389 || c == 3406))))
              : (c <= 3414 || (c < 3507
                ? (c < 3461
                  ? (c < 3450
                    ? (c >= 3423 && c <= 3425)
                    : c <= 3455)
                  : (c <= 3478 || (c >= 3482 && c <= 3505)))
                : (c <= 3515 || (c < 3585
                  ? (c < 3520
                    ? c == 3517
                    : c <= 3526)
                  : (c <= 3632 || c == 3634))))))
            : (c <= 3654 || (c < 3782
              ? (c < 3749
                ? (c < 3718
                  ? (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)
                  : (c <= 3722 || (c >= 3724 && c <= 3747)))
                : (c <= 3749 || (c < 3773
                  ? (c < 3762
                    ? (c >= 3751 && c <= 3760)
                    : c <= 3762)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))
              : (c <= 3782 || (c < 3976
                ? (c < 3904
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3911 || (c >= 3913 && c <= 3948)))
                : (c <= 3980 || (c < 4176
                  ? (c < 4159
                    ? (c >= 4096 && c <= 4138)
                    : c <= 4159)
                  : (c <= 4181 || (c >= 4186 && c <= 4189)))))))))))))
      : (c <= 4193 || (c < 8134
        ? (c < 6176
          ? (c < 4808
            ? (c < 4688
              ? (c < 4295
                ? (c < 4213
                  ? (c < 4206
                    ? (c >= 4197 && c <= 4198)
                    : c <= 4208)
                  : (c <= 4225 || (c < 4256
                    ? c == 4238
                    : c <= 4293)))
                : (c <= 4295 || (c < 4348
                  ? (c < 4304
                    ? c == 4301
                    : c <= 4346)
                  : (c <= 4680 || (c >= 4682 && c <= 4685)))))
              : (c <= 4694 || (c < 4752
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c >= 4746 && c <= 4749)))
                : (c <= 4784 || (c < 4800
                  ? (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)
                  : (c <= 4800 || (c >= 4802 && c <= 4805)))))))
            : (c <= 4822 || (c < 5792
              ? (c < 5024
                ? (c < 4888
                  ? (c < 4882
                    ? (c >= 4824 && c <= 4880)
                    : c <= 4885)
                  : (c <= 4954 || (c >= 4992 && c <= 5007)))
                : (c <= 5109 || (c < 5743
                  ? (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)
                  : (c <= 5759 || (c >= 5761 && c <= 5786)))))
              : (c <= 5866 || (c < 5984
                ? (c < 5919
                  ? (c < 5888
                    ? (c >= 5870 && c <= 5880)
                    : c <= 5905)
                  : (c <= 5937 || (c >= 5952 && c <= 5969)))
                : (c <= 5996 || (c < 6103
                  ? (c < 6016
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6067)
                  : (c <= 6103 || c == 6108))))))))
          : (c <= 6264 || (c < 7312
            ? (c < 6823
              ? (c < 6512
                ? (c < 6320
                  ? (c < 6314
                    ? (c >= 6272 && c <= 6312)
                    : c <= 6314)
                  : (c <= 6389 || (c < 6480
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6509)))
                : (c <= 6516 || (c < 6656
                  ? (c < 6576
                    ? (c >= 6528 && c <= 6571)
                    : c <= 6601)
                  : (c <= 6678 || (c >= 6688 && c <= 6740)))))
              : (c <= 6823 || (c < 7098
                ? (c < 7043
                  ? (c < 6981
                    ? (c >= 6917 && c <= 6963)
                    : c <= 6988)
                  : (c <= 7072 || (c >= 7086 && c <= 7087)))
                : (c <= 7141 || (c < 7258
                  ? (c < 7245
                    ? (c >= 7168 && c <= 7203)
                    : c <= 7247)
                  : (c <= 7293 || (c >= 7296 && c <= 7304)))))))
            : (c <= 7354 || (c < 8008
              ? (c < 7418
                ? (c < 7406
                  ? (c < 7401
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7404)
                  : (c <= 7411 || (c >= 7413 && c <= 7414)))
                : (c <= 7418 || (c < 7960
                  ? (c < 7680
                    ? (c >= 7424 && c <= 7615)
                    : c <= 7957)
                  : (c <= 7965 || (c >= 7968 && c <= 8005)))))
              : (c <= 8013 || (c < 8031
                ? (c < 8027
                  ? (c < 8025
                    ? (c >= 8016 && c <= 8023)
                    : c <= 8025)
                  : (c <= 8027 || c == 8029))
                : (c <= 8061 || (c < 8126
                  ? (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)
                  : (c <= 8126 || (c >= 8130 && c <= 8132)))))))))))
        : (c <= 8140 || (c < 12337
          ? (c < 8544
            ? (c < 8458
              ? (c < 8305
                ? (c < 8160
                  ? (c < 8150
                    ? (c >= 8144 && c <= 8147)
                    : c <= 8155)
                  : (c <= 8172 || (c < 8182
                    ? (c >= 8178 && c <= 8180)
                    : c <= 8188)))
                : (c <= 8305 || (c < 8450
                  ? (c < 8336
                    ? c == 8319
                    : c <= 8348)
                  : (c <= 8450 || c == 8455))))
              : (c <= 8467 || (c < 8488
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || c == 8486))
                : (c <= 8488 || (c < 8517
                  ? (c < 8508
                    ? (c >= 8490 && c <= 8505)
                    : c <= 8511)
                  : (c <= 8521 || c == 8526))))))
            : (c <= 8584 || (c < 11680
              ? (c < 11559
                ? (c < 11506
                  ? (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11502)
                  : (c <= 11507 || (c >= 11520 && c <= 11557)))
                : (c <= 11559 || (c < 11631
                  ? (c < 11568
                    ? c == 11565
                    : c <= 11623)
                  : (c <= 11631 || (c >= 11648 && c <= 11670)))))
              : (c <= 11686 || (c < 11720
                ? (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c >= 11712 && c <= 11718)))
                : (c <= 11726 || (c < 12293
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 12295 || (c >= 12321 && c <= 12329)))))))))
          : (c <= 12341 || (c < 42891
            ? (c < 19968
              ? (c < 12549
                ? (c < 12445
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))
                : (c <= 12591 || (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c >= 13312 && c <= 19903)))))
              : (c <= 42124 || (c < 42560
                ? (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42527 || (c >= 42538 && c <= 42539)))
                : (c <= 42606 || (c < 42775
                  ? (c < 42656
                    ? (c >= 42623 && c <= 42653)
                    : c <= 42735)
                  : (c <= 42783 || (c >= 42786 && c <= 42888)))))))
            : (c <= 42954 || (c < 43250
              ? (c < 43011
                ? (c < 42965
                  ? (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)
                  : (c <= 42969 || (c >= 42994 && c <= 43009)))
                : (c <= 43013 || (c < 43072
                  ? (c < 43020
                    ? (c >= 43015 && c <= 43018)
                    : c <= 43042)
                  : (c <= 43123 || (c >= 43138 && c <= 43187)))))
              : (c <= 43255 || (c < 43360
                ? (c < 43274
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43262)
                  : (c <= 43301 || (c >= 43312 && c <= 43334)))
                : (c <= 43388 || (c < 43488
                  ? (c < 43471
                    ? (c >= 43396 && c <= 43442)
                    : c <= 43471)
                  : (c <= 43492 || (c >= 43494 && c <= 43503)))))))))))))))
    : (c <= 43518 || (c < 70727
      ? (c < 66956
        ? (c < 64914
          ? (c < 43868
            ? (c < 43714
              ? (c < 43646
                ? (c < 43588
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43560)
                    : c <= 43586)
                  : (c <= 43595 || (c < 43642
                    ? (c >= 43616 && c <= 43638)
                    : c <= 43642)))
                : (c <= 43695 || (c < 43705
                  ? (c < 43701
                    ? c == 43697
                    : c <= 43702)
                  : (c <= 43709 || c == 43712))))
              : (c <= 43714 || (c < 43785
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43754)
                  : (c <= 43764 || (c >= 43777 && c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c >= 43824 && c <= 43866)))))))
            : (c <= 43881 || (c < 64287
              ? (c < 63744
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 43888 && c <= 44002)
                    : c <= 55203)
                  : (c <= 55238 || (c >= 55243 && c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || c == 64285))))
              : (c <= 64296 || (c < 64323
                ? (c < 64318
                  ? (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)
                  : (c <= 64318 || (c >= 64320 && c <= 64321)))
                : (c <= 64324 || (c < 64612
                  ? (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)
                  : (c <= 64829 || (c >= 64848 && c <= 64911)))))))))
          : (c <= 64967 || (c < 65599
            ? (c < 65382
              ? (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65008 && c <= 65017)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65313
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65338 || (c >= 65345 && c <= 65370)))))
              : (c <= 65437 || (c < 65498
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65440 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c >= 65490 && c <= 65495)))
                : (c <= 65500 || (c < 65576
                  ? (c < 65549
                    ? (c >= 65536 && c <= 65547)
                    : c <= 65574)
                  : (c <= 65594 || (c >= 65596 && c <= 65597)))))))
            : (c <= 65613 || (c < 66464
              ? (c < 66208
                ? (c < 65856
                  ? (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)
                  : (c <= 65908 || (c >= 66176 && c <= 66204)))
                : (c <= 66256 || (c < 66384
                  ? (c < 66349
                    ? (c >= 66304 && c <= 66335)
                    : c <= 66378)
                  : (c <= 66421 || (c >= 66432 && c <= 66461)))))
              : (c <= 66499 || (c < 66776
                ? (c < 66560
                  ? (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)
                  : (c <= 66717 || (c >= 66736 && c <= 66771)))
                : (c <= 66811 || (c < 66928
                  ? (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)
                  : (c <= 66938 || (c >= 66940 && c <= 66954)))))))))))
        : (c <= 66962 || (c < 68864
          ? (c < 67828
            ? (c < 67506
              ? (c < 67072
                ? (c < 66979
                  ? (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)
                  : (c <= 66993 || (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)))
                : (c <= 67382 || (c < 67456
                  ? (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)
                  : (c <= 67461 || (c >= 67463 && c <= 67504)))))
              : (c <= 67514 || (c < 67644
                ? (c < 67594
                  ? (c < 67592
                    ? (c >= 67584 && c <= 67589)
                    : c <= 67592)
                  : (c <= 67637 || (c >= 67639 && c <= 67640)))
                : (c <= 67644 || (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c >= 67808 && c <= 67826)))))))
            : (c <= 67829 || (c < 68224
              ? (c < 68096
                ? (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c >= 68030 && c <= 68031)))
                : (c <= 68096 || (c < 68121
                  ? (c < 68117
                    ? (c >= 68112 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c >= 68192 && c <= 68220)))))
              : (c <= 68252 || (c < 68448
                ? (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68324)
                  : (c <= 68405 || (c >= 68416 && c <= 68437)))
                : (c <= 68466 || (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c >= 68800 && c <= 68850)))))))))
          : (c <= 68899 || (c < 70106
            ? (c < 69749
              ? (c < 69488
                ? (c < 69376
                  ? (c < 69296
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69445)))
                : (c <= 69505 || (c < 69635
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69687 || (c >= 69745 && c <= 69746)))))
              : (c <= 69749 || (c < 69959
                ? (c < 69891
                  ? (c < 69840
                    ? (c >= 69763 && c <= 69807)
                    : c <= 69864)
                  : (c <= 69926 || c == 69956))
                : (c <= 69959 || (c < 70019
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70002)
                    : c <= 70006)
                  : (c <= 70066 || (c >= 70081 && c <= 70084)))))))
            : (c <= 70106 || (c < 70405
              ? (c < 70280
                ? (c < 70163
                  ? (c < 70144
                    ? c == 70108
                    : c <= 70161)
                  : (c <= 70187 || (c >= 70272 && c <= 70278)))
                : (c <= 70280 || (c < 70303
                  ? (c < 70287
                    ? (c >= 70282 && c <= 70285)
                    : c <= 70301)
                  : (c <= 70312 || (c >= 70320 && c <= 70366)))))
              : (c <= 70412 || (c < 70453
                ? (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c >= 70450 && c <= 70451)))
                : (c <= 70457 || (c < 70493
                  ? (c < 70480
                    ? c == 70461
                    : c <= 70480)
                  : (c <= 70497 || (c >= 70656 && c <= 70708)))))))))))))
      : (c <= 70730 || (c < 119894
        ? (c < 73056
          ? (c < 72001
            ? (c < 71424
              ? (c < 71128
                ? (c < 70852
                  ? (c < 70784
                    ? (c >= 70751 && c <= 70753)
                    : c <= 70831)
                  : (c <= 70853 || (c < 71040
                    ? c == 70855
                    : c <= 71086)))
                : (c <= 71131 || (c < 71296
                  ? (c < 71236
                    ? (c >= 71168 && c <= 71215)
                    : c <= 71236)
                  : (c <= 71338 || c == 71352))))
              : (c <= 71450 || (c < 71945
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71723)
                  : (c <= 71903 || (c >= 71935 && c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71983 || c == 71999))))))
            : (c <= 72001 || (c < 72349
              ? (c < 72192
                ? (c < 72161
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72144)
                  : (c <= 72161 || c == 72163))
                : (c <= 72192 || (c < 72272
                  ? (c < 72250
                    ? (c >= 72203 && c <= 72242)
                    : c <= 72250)
                  : (c <= 72272 || (c >= 72284 && c <= 72329)))))
              : (c <= 72349 || (c < 72818
                ? (c < 72714
                  ? (c < 72704
                    ? (c >= 72368 && c <= 72440)
                    : c <= 72712)
                  : (c <= 72750 || c == 72768))
                : (c <= 72847 || (c < 72971
                  ? (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)
                  : (c <= 73008 || c == 73030))))))))
          : (c <= 73061 || (c < 93952
            ? (c < 82944
              ? (c < 73728
                ? (c < 73112
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73097)
                  : (c <= 73112 || (c < 73648
                    ? (c >= 73440 && c <= 73458)
                    : c <= 73648)))
                : (c <= 74649 || (c < 77712
                  ? (c < 74880
                    ? (c >= 74752 && c <= 74862)
                    : c <= 75075)
                  : (c <= 77808 || (c >= 77824 && c <= 78894)))))
              : (c <= 83526 || (c < 92928
                ? (c < 92784
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92862 || (c >= 92880 && c <= 92909)))
                : (c <= 92975 || (c < 93053
                  ? (c < 93027
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93047)
                  : (c <= 93071 || (c >= 93760 && c <= 93823)))))))
            : (c <= 94026 || (c < 110589
              ? (c < 94208
                ? (c < 94176
                  ? (c < 94099
                    ? c == 94032
                    : c <= 94111)
                  : (c <= 94177 || c == 94179))
                : (c <= 100343 || (c < 110576
                  ? (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)
                  : (c <= 110579 || (c >= 110581 && c <= 110587)))))
              : (c <= 110590 || (c < 113664
                ? (c < 110948
                  ? (c < 110928
                    ? (c >= 110592 && c <= 110882)
                    : c <= 110930)
                  : (c <= 110951 || (c >= 110960 && c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c >= 119808 && c <= 119892)))))))))))
        : (c <= 119964 || (c < 125259
          ? (c < 120572
            ? (c < 120086
              ? (c < 119995
                ? (c < 119973
                  ? (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)
                  : (c <= 119974 || (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)))
                : (c <= 119995 || (c < 120071
                  ? (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)
                  : (c <= 120074 || (c >= 120077 && c <= 120084)))))
              : (c <= 120092 || (c < 120138
                ? (c < 120128
                  ? (c < 120123
                    ? (c >= 120094 && c <= 120121)
                    : c <= 120126)
                  : (c <= 120132 || c == 120134))
                : (c <= 120144 || (c < 120514
                  ? (c < 120488
                    ? (c >= 120146 && c <= 120485)
                    : c <= 120512)
                  : (c <= 120538 || (c >= 120540 && c <= 120570)))))))
            : (c <= 120596 || (c < 123191
              ? (c < 120714
                ? (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c >= 120688 && c <= 120712)))
                : (c <= 120744 || (c < 122624
                  ? (c < 120772
                    ? (c >= 120746 && c <= 120770)
                    : c <= 120779)
                  : (c <= 122654 || (c >= 123136 && c <= 123180)))))
              : (c <= 123197 || (c < 124904
                ? (c < 123584
                  ? (c < 123536
                    ? c == 123214
                    : c <= 123565)
                  : (c <= 123627 || (c >= 124896 && c <= 124902)))
                : (c <= 124907 || (c < 124928
                  ? (c < 124912
                    ? (c >= 124909 && c <= 124910)
                    : c <= 124926)
                  : (c <= 125124 || (c >= 125184 && c <= 125251)))))))))
          : (c <= 125259 || (c < 126559
            ? (c < 126535
              ? (c < 126505
                ? (c < 126497
                  ? (c < 126469
                    ? (c >= 126464 && c <= 126467)
                    : c <= 126495)
                  : (c <= 126498 || (c < 126503
                    ? c == 126500
                    : c <= 126503)))
                : (c <= 126514 || (c < 126523
                  ? (c < 126521
                    ? (c >= 126516 && c <= 126519)
                    : c <= 126521)
                  : (c <= 126523 || c == 126530))))
              : (c <= 126535 || (c < 126548
                ? (c < 126541
                  ? (c < 126539
                    ? c == 126537
                    : c <= 126539)
                  : (c <= 126543 || (c >= 126545 && c <= 126546)))
                : (c <= 126548 || (c < 126555
                  ? (c < 126553
                    ? c == 126551
                    : c <= 126553)
                  : (c <= 126555 || c == 126557))))))
            : (c <= 126559 || (c < 126625
              ? (c < 126580
                ? (c < 126567
                  ? (c < 126564
                    ? (c >= 126561 && c <= 126562)
                    : c <= 126564)
                  : (c <= 126570 || (c >= 126572 && c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c >= 126603 && c <= 126619)))))
              : (c <= 126627 || (c < 177984
                ? (c < 131072
                  ? (c < 126635
                    ? (c >= 126629 && c <= 126633)
                    : c <= 126651)
                  : (c <= 173791 || (c >= 173824 && c <= 177976)))
                : (c <= 178205 || (c < 194560
                  ? (c < 183984
                    ? (c >= 178208 && c <= 183969)
                    : c <= 191456)
                  : (c <= 195101 || (c >= 196608 && c <= 201546)))))))))))))))));
}

static inline bool sym_identifier_character_set_2(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'a' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static inline bool sym_identifier_character_set_3(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'b' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(92);
      if (lookahead == '!') ADVANCE(136);
      if (lookahead == '#') ADVANCE(93);
      if (lookahead == '%') ADVANCE(140);
      if (lookahead == '&') ADVANCE(4);
      if (lookahead == '(') ADVANCE(100);
      if (lookahead == ')') ADVANCE(101);
      if (lookahead == '*') ADVANCE(138);
      if (lookahead == '+') ADVANCE(139);
      if (lookahead == ',') ADVANCE(116);
      if (lookahead == '-') ADVANCE(135);
      if (lookahead == '.') ADVANCE(133);
      if (lookahead == '/') ADVANCE(137);
      if (lookahead == ':') ADVANCE(104);
      if (lookahead == ';') ADVANCE(97);
      if (lookahead == '<') ADVANCE(109);
      if (lookahead == '=') ADVANCE(121);
      if (lookahead == '>') ADVANCE(111);
      if (lookahead == '[') ADVANCE(94);
      if (lookahead == ']') ADVANCE(95);
      if (lookahead == 'b') ADVANCE(58);
      if (lookahead == 'c') ADVANCE(55);
      if (lookahead == 'e') ADVANCE(39);
      if (lookahead == 'f') ADVANCE(12);
      if (lookahead == 'i') ADVANCE(35);
      if (lookahead == 'l') ADVANCE(29);
      if (lookahead == 'm') ADVANCE(13);
      if (lookahead == 'n') ADVANCE(56);
      if (lookahead == 'o') ADVANCE(63);
      if (lookahead == 'q') ADVANCE(84);
      if (lookahead == 'r') ADVANCE(34);
      if (lookahead == 't') ADVANCE(32);
      if (lookahead == 'v') ADVANCE(22);
      if (lookahead == 'x') ADVANCE(150);
      if (lookahead == '{') ADVANCE(117);
      if (lookahead == '|') ADVANCE(90);
      if (lookahead == '}') ADVANCE(118);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(0)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(162);
      END_STATE();
    case 1:
      if (lookahead == '!') ADVANCE(136);
      if (lookahead == '%') ADVANCE(140);
      if (lookahead == '&') ADVANCE(4);
      if (lookahead == '(') ADVANCE(100);
      if (lookahead == ')') ADVANCE(101);
      if (lookahead == '*') ADVANCE(138);
      if (lookahead == '+') ADVANCE(139);
      if (lookahead == ',') ADVANCE(116);
      if (lookahead == '-') ADVANCE(134);
      if (lookahead == '.') ADVANCE(133);
      if (lookahead == '/') ADVANCE(137);
      if (lookahead == ':') ADVANCE(8);
      if (lookahead == ';') ADVANCE(97);
      if (lookahead == '<') ADVANCE(109);
      if (lookahead == '=') ADVANCE(10);
      if (lookahead == '>') ADVANCE(111);
      if (lookahead == '[') ADVANCE(94);
      if (lookahead == ']') ADVANCE(95);
      if (lookahead == 'c') ADVANCE(177);
      if (lookahead == 'e') ADVANCE(182);
      if (lookahead == 'f') ADVANCE(163);
      if (lookahead == 'i') ADVANCE(170);
      if (lookahead == 'l') ADVANCE(166);
      if (lookahead == 't') ADVANCE(175);
      if (lookahead == '{') ADVANCE(117);
      if (lookahead == '|') ADVANCE(90);
      if (lookahead == '}') ADVANCE(118);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(1)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(162);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(183);
      END_STATE();
    case 2:
      if (lookahead == '!') ADVANCE(136);
      if (lookahead == '%') ADVANCE(140);
      if (lookahead == '&') ADVANCE(4);
      if (lookahead == '(') ADVANCE(100);
      if (lookahead == ')') ADVANCE(101);
      if (lookahead == '*') ADVANCE(138);
      if (lookahead == '+') ADVANCE(139);
      if (lookahead == ',') ADVANCE(116);
      if (lookahead == '-') ADVANCE(134);
      if (lookahead == '.') ADVANCE(132);
      if (lookahead == '/') ADVANCE(137);
      if (lookahead == ';') ADVANCE(97);
      if (lookahead == '<') ADVANCE(109);
      if (lookahead == '=') ADVANCE(121);
      if (lookahead == '>') ADVANCE(111);
      if (lookahead == '[') ADVANCE(94);
      if (lookahead == ']') ADVANCE(95);
      if (lookahead == 'e') ADVANCE(182);
      if (lookahead == 'f') ADVANCE(164);
      if (lookahead == 'i') ADVANCE(170);
      if (lookahead == 't') ADVANCE(175);
      if (lookahead == '{') ADVANCE(117);
      if (lookahead == '|') ADVANCE(90);
      if (lookahead == '}') ADVANCE(118);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(2)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(162);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(183);
      END_STATE();
    case 3:
      if (lookahead == '!') ADVANCE(136);
      if (lookahead == '%') ADVANCE(140);
      if (lookahead == '&') ADVANCE(4);
      if (lookahead == '(') ADVANCE(100);
      if (lookahead == '*') ADVANCE(138);
      if (lookahead == '+') ADVANCE(139);
      if (lookahead == '-') ADVANCE(134);
      if (lookahead == '/') ADVANCE(137);
      if (lookahead == '<') ADVANCE(109);
      if (lookahead == '=') ADVANCE(10);
      if (lookahead == '>') ADVANCE(111);
      if (lookahead == '[') ADVANCE(94);
      if (lookahead == 'c') ADVANCE(177);
      if (lookahead == 'e') ADVANCE(174);
      if (lookahead == 'f') ADVANCE(163);
      if (lookahead == 'i') ADVANCE(170);
      if (lookahead == 'l') ADVANCE(166);
      if (lookahead == 't') ADVANCE(175);
      if (lookahead == '|') ADVANCE(90);
      if (lookahead == '}') ADVANCE(118);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(3)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(162);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(183);
      END_STATE();
    case 4:
      if (lookahead == '&') ADVANCE(145);
      END_STATE();
    case 5:
      if (lookahead == ')') ADVANCE(101);
      if (lookahead == ',') ADVANCE(116);
      if (lookahead == '-') ADVANCE(11);
      if (lookahead == ';') ADVANCE(97);
      if (lookahead == '<') ADVANCE(108);
      if (lookahead == '>') ADVANCE(110);
      if (lookahead == 'x') ADVANCE(150);
      if (lookahead == '{') ADVANCE(117);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(5)
      END_STATE();
    case 6:
      if (lookahead == ')') ADVANCE(101);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(6)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(162);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(183);
      END_STATE();
    case 7:
      if (lookahead == '.') ADVANCE(129);
      END_STATE();
    case 8:
      if (lookahead == ':') ADVANCE(98);
      END_STATE();
    case 9:
      if (lookahead == '=') ADVANCE(142);
      END_STATE();
    case 10:
      if (lookahead == '=') ADVANCE(141);
      END_STATE();
    case 11:
      if (lookahead == '>') ADVANCE(105);
      END_STATE();
    case 12:
      if (lookahead == 'a') ADVANCE(48);
      if (lookahead == 'n') ADVANCE(115);
      if (lookahead == 'o') ADVANCE(67);
      END_STATE();
    case 13:
      if (lookahead == 'a') ADVANCE(77);
      if (lookahead == 'o') ADVANCE(21);
      END_STATE();
    case 14:
      if (lookahead == 'a') ADVANCE(42);
      END_STATE();
    case 15:
      if (lookahead == 'a') ADVANCE(78);
      END_STATE();
    case 16:
      if (lookahead == 'a') ADVANCE(44);
      END_STATE();
    case 17:
      if (lookahead == 'a') ADVANCE(45);
      END_STATE();
    case 18:
      if (lookahead == 'a') ADVANCE(83);
      END_STATE();
    case 19:
      if (lookahead == 'c') ADVANCE(148);
      END_STATE();
    case 20:
      if (lookahead == 'c') ADVANCE(33);
      END_STATE();
    case 21:
      if (lookahead == 'd') ADVANCE(86);
      END_STATE();
    case 22:
      if (lookahead == 'e') ADVANCE(19);
      END_STATE();
    case 23:
      if (lookahead == 'e') ADVANCE(126);
      END_STATE();
    case 24:
      if (lookahead == 'e') ADVANCE(157);
      END_STATE();
    case 25:
      if (lookahead == 'e') ADVANCE(158);
      END_STATE();
    case 26:
      if (lookahead == 'e') ADVANCE(160);
      END_STATE();
    case 27:
      if (lookahead == 'e') ADVANCE(88);
      END_STATE();
    case 28:
      if (lookahead == 'e') ADVANCE(96);
      END_STATE();
    case 29:
      if (lookahead == 'e') ADVANCE(76);
      END_STATE();
    case 30:
      if (lookahead == 'e') ADVANCE(70);
      END_STATE();
    case 31:
      if (lookahead == 'e') ADVANCE(53);
      END_STATE();
    case 32:
      if (lookahead == 'e') ADVANCE(53);
      if (lookahead == 'r') ADVANCE(85);
      END_STATE();
    case 33:
      if (lookahead == 'e') ADVANCE(66);
      END_STATE();
    case 34:
      if (lookahead == 'e') ADVANCE(16);
      END_STATE();
    case 35:
      if (lookahead == 'f') ADVANCE(124);
      if (lookahead == 'm') ADVANCE(64);
      if (lookahead == 'n') ADVANCE(128);
      END_STATE();
    case 36:
      if (lookahead == 'g') ADVANCE(122);
      END_STATE();
    case 37:
      if (lookahead == 'i') ADVANCE(79);
      END_STATE();
    case 38:
      if (lookahead == 'i') ADVANCE(60);
      END_STATE();
    case 39:
      if (lookahead == 'l') ADVANCE(73);
      if (lookahead == 'n') ADVANCE(75);
      if (lookahead == 'v') ADVANCE(14);
      if (lookahead == 'x') ADVANCE(62);
      END_STATE();
    case 40:
      if (lookahead == 'l') ADVANCE(73);
      if (lookahead == 'n') ADVANCE(75);
      if (lookahead == 'x') ADVANCE(62);
      END_STATE();
    case 41:
      if (lookahead == 'l') ADVANCE(156);
      END_STATE();
    case 42:
      if (lookahead == 'l') ADVANCE(130);
      END_STATE();
    case 43:
      if (lookahead == 'l') ADVANCE(107);
      END_STATE();
    case 44:
      if (lookahead == 'l') ADVANCE(152);
      END_STATE();
    case 45:
      if (lookahead == 'l') ADVANCE(147);
      END_STATE();
    case 46:
      if (lookahead == 'l') ADVANCE(27);
      END_STATE();
    case 47:
      if (lookahead == 'l') ADVANCE(28);
      END_STATE();
    case 48:
      if (lookahead == 'l') ADVANCE(74);
      END_STATE();
    case 49:
      if (lookahead == 'm') ADVANCE(64);
      if (lookahead == 'n') ADVANCE(82);
      END_STATE();
    case 50:
      if (lookahead == 'm') ADVANCE(65);
      if (lookahead == 'n') ADVANCE(20);
      END_STATE();
    case 51:
      if (lookahead == 'n') ADVANCE(115);
      END_STATE();
    case 52:
      if (lookahead == 'n') ADVANCE(106);
      END_STATE();
    case 53:
      if (lookahead == 'n') ADVANCE(72);
      END_STATE();
    case 54:
      if (lookahead == 'n') ADVANCE(24);
      END_STATE();
    case 55:
      if (lookahead == 'o') ADVANCE(50);
      if (lookahead == 's') ADVANCE(36);
      END_STATE();
    case 56:
      if (lookahead == 'o') ADVANCE(54);
      END_STATE();
    case 57:
      if (lookahead == 'o') ADVANCE(41);
      END_STATE();
    case 58:
      if (lookahead == 'o') ADVANCE(57);
      END_STATE();
    case 59:
      if (lookahead == 'o') ADVANCE(71);
      END_STATE();
    case 60:
      if (lookahead == 'o') ADVANCE(52);
      END_STATE();
    case 61:
      if (lookahead == 'o') ADVANCE(68);
      END_STATE();
    case 62:
      if (lookahead == 'p') ADVANCE(59);
      END_STATE();
    case 63:
      if (lookahead == 'p') ADVANCE(30);
      END_STATE();
    case 64:
      if (lookahead == 'p') ADVANCE(43);
      END_STATE();
    case 65:
      if (lookahead == 'p') ADVANCE(46);
      END_STATE();
    case 66:
      if (lookahead == 'p') ADVANCE(81);
      END_STATE();
    case 67:
      if (lookahead == 'r') ADVANCE(112);
      END_STATE();
    case 68:
      if (lookahead == 'r') ADVANCE(151);
      END_STATE();
    case 69:
      if (lookahead == 'r') ADVANCE(87);
      END_STATE();
    case 70:
      if (lookahead == 'r') ADVANCE(18);
      END_STATE();
    case 71:
      if (lookahead == 'r') ADVANCE(80);
      END_STATE();
    case 72:
      if (lookahead == 's') ADVANCE(61);
      END_STATE();
    case 73:
      if (lookahead == 's') ADVANCE(23);
      END_STATE();
    case 74:
      if (lookahead == 's') ADVANCE(26);
      END_STATE();
    case 75:
      if (lookahead == 't') ADVANCE(37);
      END_STATE();
    case 76:
      if (lookahead == 't') ADVANCE(119);
      END_STATE();
    case 77:
      if (lookahead == 't') ADVANCE(149);
      END_STATE();
    case 78:
      if (lookahead == 't') ADVANCE(155);
      END_STATE();
    case 79:
      if (lookahead == 't') ADVANCE(89);
      END_STATE();
    case 80:
      if (lookahead == 't') ADVANCE(114);
      END_STATE();
    case 81:
      if (lookahead == 't') ADVANCE(102);
      END_STATE();
    case 82:
      if (lookahead == 't') ADVANCE(153);
      END_STATE();
    case 83:
      if (lookahead == 't') ADVANCE(38);
      END_STATE();
    case 84:
      if (lookahead == 'u') ADVANCE(15);
      END_STATE();
    case 85:
      if (lookahead == 'u') ADVANCE(25);
      END_STATE();
    case 86:
      if (lookahead == 'u') ADVANCE(47);
      END_STATE();
    case 87:
      if (lookahead == 'v') ADVANCE(17);
      END_STATE();
    case 88:
      if (lookahead == 'x') ADVANCE(154);
      END_STATE();
    case 89:
      if (lookahead == 'y') ADVANCE(99);
      END_STATE();
    case 90:
      if (lookahead == '|') ADVANCE(146);
      END_STATE();
    case 91:
      if (eof) ADVANCE(92);
      if (lookahead == '!') ADVANCE(9);
      if (lookahead == '#') ADVANCE(93);
      if (lookahead == '%') ADVANCE(140);
      if (lookahead == '&') ADVANCE(4);
      if (lookahead == '(') ADVANCE(100);
      if (lookahead == ')') ADVANCE(101);
      if (lookahead == '*') ADVANCE(138);
      if (lookahead == '+') ADVANCE(139);
      if (lookahead == ',') ADVANCE(116);
      if (lookahead == '-') ADVANCE(134);
      if (lookahead == '.') ADVANCE(7);
      if (lookahead == '/') ADVANCE(137);
      if (lookahead == ':') ADVANCE(103);
      if (lookahead == ';') ADVANCE(97);
      if (lookahead == '<') ADVANCE(109);
      if (lookahead == '=') ADVANCE(10);
      if (lookahead == '>') ADVANCE(111);
      if (lookahead == ']') ADVANCE(95);
      if (lookahead == 'b') ADVANCE(58);
      if (lookahead == 'c') ADVANCE(55);
      if (lookahead == 'e') ADVANCE(40);
      if (lookahead == 'f') ADVANCE(51);
      if (lookahead == 'i') ADVANCE(49);
      if (lookahead == 'm') ADVANCE(13);
      if (lookahead == 'n') ADVANCE(56);
      if (lookahead == 'o') ADVANCE(63);
      if (lookahead == 'q') ADVANCE(84);
      if (lookahead == 'r') ADVANCE(34);
      if (lookahead == 't') ADVANCE(31);
      if (lookahead == 'v') ADVANCE(22);
      if (lookahead == '{') ADVANCE(117);
      if (lookahead == '|') ADVANCE(90);
      if (lookahead == '}') ADVANCE(118);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(91)
      END_STATE();
    case 92:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 93:
      ACCEPT_TOKEN(anon_sym_POUND);
      END_STATE();
    case 94:
      ACCEPT_TOKEN(anon_sym_LBRACK);
      END_STATE();
    case 95:
      ACCEPT_TOKEN(anon_sym_RBRACK);
      END_STATE();
    case 96:
      ACCEPT_TOKEN(anon_sym_module);
      END_STATE();
    case 97:
      ACCEPT_TOKEN(anon_sym_SEMI);
      END_STATE();
    case 98:
      ACCEPT_TOKEN(anon_sym_COLON_COLON);
      END_STATE();
    case 99:
      ACCEPT_TOKEN(anon_sym_entity);
      END_STATE();
    case 100:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 101:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 102:
      ACCEPT_TOKEN(anon_sym_concept);
      END_STATE();
    case 103:
      ACCEPT_TOKEN(anon_sym_COLON);
      END_STATE();
    case 104:
      ACCEPT_TOKEN(anon_sym_COLON);
      if (lookahead == ':') ADVANCE(98);
      END_STATE();
    case 105:
      ACCEPT_TOKEN(anon_sym_DASH_GT);
      END_STATE();
    case 106:
      ACCEPT_TOKEN(anon_sym_operation);
      END_STATE();
    case 107:
      ACCEPT_TOKEN(anon_sym_impl);
      END_STATE();
    case 108:
      ACCEPT_TOKEN(anon_sym_LT);
      END_STATE();
    case 109:
      ACCEPT_TOKEN(anon_sym_LT);
      if (lookahead == '=') ADVANCE(143);
      END_STATE();
    case 110:
      ACCEPT_TOKEN(anon_sym_GT);
      END_STATE();
    case 111:
      ACCEPT_TOKEN(anon_sym_GT);
      if (lookahead == '=') ADVANCE(144);
      END_STATE();
    case 112:
      ACCEPT_TOKEN(anon_sym_for);
      END_STATE();
    case 113:
      ACCEPT_TOKEN(anon_sym_for);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 114:
      ACCEPT_TOKEN(anon_sym_export);
      END_STATE();
    case 115:
      ACCEPT_TOKEN(anon_sym_fn);
      END_STATE();
    case 116:
      ACCEPT_TOKEN(anon_sym_COMMA);
      END_STATE();
    case 117:
      ACCEPT_TOKEN(anon_sym_LBRACE);
      END_STATE();
    case 118:
      ACCEPT_TOKEN(anon_sym_RBRACE);
      END_STATE();
    case 119:
      ACCEPT_TOKEN(anon_sym_let);
      END_STATE();
    case 120:
      ACCEPT_TOKEN(anon_sym_let);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 121:
      ACCEPT_TOKEN(anon_sym_EQ);
      if (lookahead == '=') ADVANCE(141);
      END_STATE();
    case 122:
      ACCEPT_TOKEN(anon_sym_csg);
      END_STATE();
    case 123:
      ACCEPT_TOKEN(anon_sym_csg);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 124:
      ACCEPT_TOKEN(anon_sym_if);
      END_STATE();
    case 125:
      ACCEPT_TOKEN(anon_sym_if);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 126:
      ACCEPT_TOKEN(anon_sym_else);
      END_STATE();
    case 127:
      ACCEPT_TOKEN(anon_sym_else);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 128:
      ACCEPT_TOKEN(anon_sym_in);
      END_STATE();
    case 129:
      ACCEPT_TOKEN(anon_sym_DOT_DOT);
      END_STATE();
    case 130:
      ACCEPT_TOKEN(anon_sym_eval);
      END_STATE();
    case 131:
      ACCEPT_TOKEN(anon_sym_eval);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 132:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 133:
      ACCEPT_TOKEN(anon_sym_DOT);
      if (lookahead == '.') ADVANCE(129);
      END_STATE();
    case 134:
      ACCEPT_TOKEN(anon_sym_DASH);
      END_STATE();
    case 135:
      ACCEPT_TOKEN(anon_sym_DASH);
      if (lookahead == '>') ADVANCE(105);
      END_STATE();
    case 136:
      ACCEPT_TOKEN(anon_sym_BANG);
      if (lookahead == '=') ADVANCE(142);
      END_STATE();
    case 137:
      ACCEPT_TOKEN(anon_sym_SLASH);
      if (lookahead == '/') ADVANCE(184);
      END_STATE();
    case 138:
      ACCEPT_TOKEN(anon_sym_STAR);
      END_STATE();
    case 139:
      ACCEPT_TOKEN(anon_sym_PLUS);
      END_STATE();
    case 140:
      ACCEPT_TOKEN(anon_sym_PERCENT);
      END_STATE();
    case 141:
      ACCEPT_TOKEN(anon_sym_EQ_EQ);
      END_STATE();
    case 142:
      ACCEPT_TOKEN(anon_sym_BANG_EQ);
      END_STATE();
    case 143:
      ACCEPT_TOKEN(anon_sym_LT_EQ);
      END_STATE();
    case 144:
      ACCEPT_TOKEN(anon_sym_GT_EQ);
      END_STATE();
    case 145:
      ACCEPT_TOKEN(anon_sym_AMP_AMP);
      END_STATE();
    case 146:
      ACCEPT_TOKEN(anon_sym_PIPE_PIPE);
      END_STATE();
    case 147:
      ACCEPT_TOKEN(anon_sym_interval);
      END_STATE();
    case 148:
      ACCEPT_TOKEN(anon_sym_vec);
      END_STATE();
    case 149:
      ACCEPT_TOKEN(anon_sym_mat);
      END_STATE();
    case 150:
      ACCEPT_TOKEN(anon_sym_x);
      END_STATE();
    case 151:
      ACCEPT_TOKEN(anon_sym_tensor);
      END_STATE();
    case 152:
      ACCEPT_TOKEN(anon_sym_real);
      END_STATE();
    case 153:
      ACCEPT_TOKEN(anon_sym_int);
      if (lookahead == 'e') ADVANCE(69);
      END_STATE();
    case 154:
      ACCEPT_TOKEN(anon_sym_complex);
      END_STATE();
    case 155:
      ACCEPT_TOKEN(anon_sym_quat);
      END_STATE();
    case 156:
      ACCEPT_TOKEN(anon_sym_bool);
      END_STATE();
    case 157:
      ACCEPT_TOKEN(anon_sym_none);
      END_STATE();
    case 158:
      ACCEPT_TOKEN(anon_sym_true);
      END_STATE();
    case 159:
      ACCEPT_TOKEN(anon_sym_true);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 160:
      ACCEPT_TOKEN(anon_sym_false);
      END_STATE();
    case 161:
      ACCEPT_TOKEN(anon_sym_false);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 162:
      ACCEPT_TOKEN(sym_digit);
      if (('0' <= lookahead && lookahead <= '9') ||
          lookahead == '_') ADVANCE(162);
      END_STATE();
    case 163:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(173);
      if (lookahead == 'o') ADVANCE(176);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(183);
      END_STATE();
    case 164:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(173);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(183);
      END_STATE();
    case 165:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(172);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(183);
      END_STATE();
    case 166:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(180);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 167:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(159);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 168:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(161);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 169:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(127);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 170:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'f') ADVANCE(125);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 171:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'g') ADVANCE(123);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 172:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(131);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 173:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(178);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 174:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(179);
      if (lookahead == 'v') ADVANCE(165);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 175:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(181);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 176:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(113);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 177:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(171);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 178:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(168);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 179:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(169);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 180:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(120);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 181:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'u') ADVANCE(167);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 182:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'v') ADVANCE(165);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 183:
      ACCEPT_TOKEN(sym_identifier);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(183);
      END_STATE();
    case 184:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(184);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 0},
  [2] = {.lex_state = 1},
  [3] = {.lex_state = 1},
  [4] = {.lex_state = 1},
  [5] = {.lex_state = 1},
  [6] = {.lex_state = 1},
  [7] = {.lex_state = 1},
  [8] = {.lex_state = 1},
  [9] = {.lex_state = 91},
  [10] = {.lex_state = 91},
  [11] = {.lex_state = 91},
  [12] = {.lex_state = 91},
  [13] = {.lex_state = 2},
  [14] = {.lex_state = 3},
  [15] = {.lex_state = 3},
  [16] = {.lex_state = 3},
  [17] = {.lex_state = 3},
  [18] = {.lex_state = 3},
  [19] = {.lex_state = 2},
  [20] = {.lex_state = 1},
  [21] = {.lex_state = 1},
  [22] = {.lex_state = 1},
  [23] = {.lex_state = 1},
  [24] = {.lex_state = 1},
  [25] = {.lex_state = 2},
  [26] = {.lex_state = 2},
  [27] = {.lex_state = 2},
  [28] = {.lex_state = 2},
  [29] = {.lex_state = 2},
  [30] = {.lex_state = 2},
  [31] = {.lex_state = 2},
  [32] = {.lex_state = 2},
  [33] = {.lex_state = 2},
  [34] = {.lex_state = 2},
  [35] = {.lex_state = 2},
  [36] = {.lex_state = 2},
  [37] = {.lex_state = 2},
  [38] = {.lex_state = 2},
  [39] = {.lex_state = 2},
  [40] = {.lex_state = 2},
  [41] = {.lex_state = 2},
  [42] = {.lex_state = 2},
  [43] = {.lex_state = 2},
  [44] = {.lex_state = 2},
  [45] = {.lex_state = 2},
  [46] = {.lex_state = 2},
  [47] = {.lex_state = 2},
  [48] = {.lex_state = 2},
  [49] = {.lex_state = 2},
  [50] = {.lex_state = 2},
  [51] = {.lex_state = 2},
  [52] = {.lex_state = 2},
  [53] = {.lex_state = 2},
  [54] = {.lex_state = 2},
  [55] = {.lex_state = 2},
  [56] = {.lex_state = 2},
  [57] = {.lex_state = 1},
  [58] = {.lex_state = 1},
  [59] = {.lex_state = 2},
  [60] = {.lex_state = 1},
  [61] = {.lex_state = 1},
  [62] = {.lex_state = 1},
  [63] = {.lex_state = 91},
  [64] = {.lex_state = 1},
  [65] = {.lex_state = 1},
  [66] = {.lex_state = 1},
  [67] = {.lex_state = 1},
  [68] = {.lex_state = 1},
  [69] = {.lex_state = 1},
  [70] = {.lex_state = 1},
  [71] = {.lex_state = 2},
  [72] = {.lex_state = 2},
  [73] = {.lex_state = 1},
  [74] = {.lex_state = 1},
  [75] = {.lex_state = 1},
  [76] = {.lex_state = 2},
  [77] = {.lex_state = 1},
  [78] = {.lex_state = 1},
  [79] = {.lex_state = 1},
  [80] = {.lex_state = 1},
  [81] = {.lex_state = 1},
  [82] = {.lex_state = 1},
  [83] = {.lex_state = 1},
  [84] = {.lex_state = 2},
  [85] = {.lex_state = 1},
  [86] = {.lex_state = 0},
  [87] = {.lex_state = 0},
  [88] = {.lex_state = 1},
  [89] = {.lex_state = 1},
  [90] = {.lex_state = 1},
  [91] = {.lex_state = 91},
  [92] = {.lex_state = 1},
  [93] = {.lex_state = 91},
  [94] = {.lex_state = 1},
  [95] = {.lex_state = 1},
  [96] = {.lex_state = 91},
  [97] = {.lex_state = 1},
  [98] = {.lex_state = 91},
  [99] = {.lex_state = 91},
  [100] = {.lex_state = 1},
  [101] = {.lex_state = 91},
  [102] = {.lex_state = 1},
  [103] = {.lex_state = 91},
  [104] = {.lex_state = 1},
  [105] = {.lex_state = 1},
  [106] = {.lex_state = 1},
  [107] = {.lex_state = 1},
  [108] = {.lex_state = 1},
  [109] = {.lex_state = 1},
  [110] = {.lex_state = 1},
  [111] = {.lex_state = 1},
  [112] = {.lex_state = 1},
  [113] = {.lex_state = 1},
  [114] = {.lex_state = 1},
  [115] = {.lex_state = 1},
  [116] = {.lex_state = 1},
  [117] = {.lex_state = 1},
  [118] = {.lex_state = 1},
  [119] = {.lex_state = 1},
  [120] = {.lex_state = 1},
  [121] = {.lex_state = 1},
  [122] = {.lex_state = 1},
  [123] = {.lex_state = 1},
  [124] = {.lex_state = 1},
  [125] = {.lex_state = 1},
  [126] = {.lex_state = 1},
  [127] = {.lex_state = 1},
  [128] = {.lex_state = 1},
  [129] = {.lex_state = 1},
  [130] = {.lex_state = 1},
  [131] = {.lex_state = 1},
  [132] = {.lex_state = 1},
  [133] = {.lex_state = 1},
  [134] = {.lex_state = 91},
  [135] = {.lex_state = 91},
  [136] = {.lex_state = 0},
  [137] = {.lex_state = 0},
  [138] = {.lex_state = 0},
  [139] = {.lex_state = 2},
  [140] = {.lex_state = 0},
  [141] = {.lex_state = 0},
  [142] = {.lex_state = 0},
  [143] = {.lex_state = 0},
  [144] = {.lex_state = 0},
  [145] = {.lex_state = 0},
  [146] = {.lex_state = 0},
  [147] = {.lex_state = 0},
  [148] = {.lex_state = 0},
  [149] = {.lex_state = 0},
  [150] = {.lex_state = 2},
  [151] = {.lex_state = 0},
  [152] = {.lex_state = 0},
  [153] = {.lex_state = 91},
  [154] = {.lex_state = 5},
  [155] = {.lex_state = 5},
  [156] = {.lex_state = 5},
  [157] = {.lex_state = 5},
  [158] = {.lex_state = 5},
  [159] = {.lex_state = 5},
  [160] = {.lex_state = 5},
  [161] = {.lex_state = 5},
  [162] = {.lex_state = 5},
  [163] = {.lex_state = 6},
  [164] = {.lex_state = 5},
  [165] = {.lex_state = 6},
  [166] = {.lex_state = 6},
  [167] = {.lex_state = 5},
  [168] = {.lex_state = 5},
  [169] = {.lex_state = 6},
  [170] = {.lex_state = 6},
  [171] = {.lex_state = 6},
  [172] = {.lex_state = 0},
  [173] = {.lex_state = 6},
  [174] = {.lex_state = 0},
  [175] = {.lex_state = 0},
  [176] = {.lex_state = 6},
  [177] = {.lex_state = 0},
  [178] = {.lex_state = 0},
  [179] = {.lex_state = 0},
  [180] = {.lex_state = 0},
  [181] = {.lex_state = 6},
  [182] = {.lex_state = 6},
  [183] = {.lex_state = 0},
  [184] = {.lex_state = 0},
  [185] = {.lex_state = 6},
  [186] = {.lex_state = 6},
  [187] = {.lex_state = 0},
  [188] = {.lex_state = 0},
  [189] = {.lex_state = 0},
  [190] = {.lex_state = 6},
  [191] = {.lex_state = 6},
  [192] = {.lex_state = 0},
  [193] = {.lex_state = 0},
  [194] = {.lex_state = 0},
  [195] = {.lex_state = 0},
  [196] = {.lex_state = 0},
  [197] = {.lex_state = 0},
  [198] = {.lex_state = 6},
  [199] = {.lex_state = 0},
  [200] = {.lex_state = 1},
  [201] = {.lex_state = 6},
  [202] = {.lex_state = 0},
  [203] = {.lex_state = 0},
  [204] = {.lex_state = 6},
  [205] = {.lex_state = 2},
  [206] = {.lex_state = 6},
  [207] = {.lex_state = 6},
  [208] = {.lex_state = 0},
  [209] = {.lex_state = 0},
  [210] = {.lex_state = 0},
  [211] = {.lex_state = 0},
  [212] = {.lex_state = 2},
  [213] = {.lex_state = 1},
  [214] = {.lex_state = 6},
  [215] = {.lex_state = 0},
  [216] = {.lex_state = 2},
  [217] = {.lex_state = 0},
  [218] = {.lex_state = 0},
  [219] = {.lex_state = 0},
  [220] = {.lex_state = 0},
  [221] = {.lex_state = 0},
  [222] = {.lex_state = 0},
  [223] = {.lex_state = 0},
  [224] = {.lex_state = 0},
  [225] = {.lex_state = 0},
  [226] = {.lex_state = 0},
  [227] = {.lex_state = 0},
  [228] = {.lex_state = 0},
  [229] = {.lex_state = 6},
  [230] = {.lex_state = 0},
  [231] = {.lex_state = 0},
  [232] = {.lex_state = 6},
  [233] = {.lex_state = 2},
  [234] = {.lex_state = 1},
  [235] = {.lex_state = 0},
  [236] = {.lex_state = 91},
  [237] = {.lex_state = 0},
  [238] = {.lex_state = 0},
  [239] = {.lex_state = 0},
  [240] = {.lex_state = 0},
  [241] = {.lex_state = 6},
  [242] = {.lex_state = 5},
  [243] = {.lex_state = 0},
  [244] = {.lex_state = 0},
  [245] = {.lex_state = 6},
  [246] = {.lex_state = 0},
  [247] = {.lex_state = 0},
  [248] = {.lex_state = 5},
  [249] = {.lex_state = 2},
  [250] = {.lex_state = 0},
  [251] = {.lex_state = 6},
  [252] = {.lex_state = 0},
  [253] = {.lex_state = 5},
  [254] = {.lex_state = 0},
  [255] = {.lex_state = 0},
  [256] = {.lex_state = 0},
  [257] = {.lex_state = 0},
  [258] = {.lex_state = 6},
  [259] = {.lex_state = 0},
  [260] = {.lex_state = 0},
  [261] = {.lex_state = 91},
  [262] = {.lex_state = 0},
  [263] = {.lex_state = 0},
  [264] = {.lex_state = 0},
  [265] = {.lex_state = 0},
  [266] = {.lex_state = 0},
  [267] = {.lex_state = 0},
  [268] = {.lex_state = 0},
  [269] = {.lex_state = 0},
  [270] = {.lex_state = 0},
  [271] = {.lex_state = 0},
  [272] = {.lex_state = 6},
  [273] = {.lex_state = 6},
  [274] = {.lex_state = 0},
  [275] = {.lex_state = 0},
  [276] = {.lex_state = 6},
  [277] = {.lex_state = 6},
  [278] = {.lex_state = 0},
  [279] = {.lex_state = 6},
  [280] = {.lex_state = 5},
  [281] = {.lex_state = 0},
  [282] = {.lex_state = 0},
  [283] = {.lex_state = 6},
  [284] = {.lex_state = 5},
  [285] = {.lex_state = 6},
  [286] = {.lex_state = 6},
  [287] = {.lex_state = 0},
  [288] = {.lex_state = 0},
  [289] = {.lex_state = 6},
  [290] = {.lex_state = 0},
  [291] = {.lex_state = 6},
  [292] = {.lex_state = 0},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_POUND] = ACTIONS(1),
    [anon_sym_LBRACK] = ACTIONS(1),
    [anon_sym_RBRACK] = ACTIONS(1),
    [anon_sym_module] = ACTIONS(1),
    [anon_sym_SEMI] = ACTIONS(1),
    [anon_sym_COLON_COLON] = ACTIONS(1),
    [anon_sym_entity] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [anon_sym_concept] = ACTIONS(1),
    [anon_sym_COLON] = ACTIONS(1),
    [anon_sym_DASH_GT] = ACTIONS(1),
    [anon_sym_operation] = ACTIONS(1),
    [anon_sym_impl] = ACTIONS(1),
    [anon_sym_LT] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [anon_sym_for] = ACTIONS(1),
    [anon_sym_export] = ACTIONS(1),
    [anon_sym_fn] = ACTIONS(1),
    [anon_sym_COMMA] = ACTIONS(1),
    [anon_sym_LBRACE] = ACTIONS(1),
    [anon_sym_RBRACE] = ACTIONS(1),
    [anon_sym_let] = ACTIONS(1),
    [anon_sym_EQ] = ACTIONS(1),
    [anon_sym_csg] = ACTIONS(1),
    [anon_sym_if] = ACTIONS(1),
    [anon_sym_else] = ACTIONS(1),
    [anon_sym_in] = ACTIONS(1),
    [anon_sym_DOT_DOT] = ACTIONS(1),
    [anon_sym_eval] = ACTIONS(1),
    [anon_sym_DOT] = ACTIONS(1),
    [anon_sym_DASH] = ACTIONS(1),
    [anon_sym_BANG] = ACTIONS(1),
    [anon_sym_SLASH] = ACTIONS(1),
    [anon_sym_STAR] = ACTIONS(1),
    [anon_sym_PLUS] = ACTIONS(1),
    [anon_sym_PERCENT] = ACTIONS(1),
    [anon_sym_EQ_EQ] = ACTIONS(1),
    [anon_sym_BANG_EQ] = ACTIONS(1),
    [anon_sym_LT_EQ] = ACTIONS(1),
    [anon_sym_GT_EQ] = ACTIONS(1),
    [anon_sym_AMP_AMP] = ACTIONS(1),
    [anon_sym_PIPE_PIPE] = ACTIONS(1),
    [anon_sym_vec] = ACTIONS(1),
    [anon_sym_mat] = ACTIONS(1),
    [anon_sym_x] = ACTIONS(1),
    [anon_sym_tensor] = ACTIONS(1),
    [anon_sym_real] = ACTIONS(1),
    [anon_sym_complex] = ACTIONS(1),
    [anon_sym_quat] = ACTIONS(1),
    [anon_sym_bool] = ACTIONS(1),
    [anon_sym_none] = ACTIONS(1),
    [anon_sym_true] = ACTIONS(1),
    [anon_sym_false] = ACTIONS(1),
    [sym_digit] = ACTIONS(1),
    [sym_comment] = ACTIONS(1),
  },
  [1] = {
    [sym_source_file] = STATE(271),
    [sym_ct_attrib] = STATE(87),
    [sym_module] = STATE(87),
    [sym_def_entity] = STATE(87),
    [sym_def_concept] = STATE(87),
    [sym_def_operation] = STATE(87),
    [sym_impl_block] = STATE(87),
    [sym_fn] = STATE(87),
    [aux_sym_source_file_repeat1] = STATE(87),
    [ts_builtin_sym_end] = ACTIONS(3),
    [anon_sym_POUND] = ACTIONS(5),
    [anon_sym_module] = ACTIONS(7),
    [anon_sym_entity] = ACTIONS(9),
    [anon_sym_concept] = ACTIONS(11),
    [anon_sym_operation] = ACTIONS(13),
    [anon_sym_impl] = ACTIONS(15),
    [anon_sym_export] = ACTIONS(17),
    [anon_sym_fn] = ACTIONS(19),
    [sym_comment] = ACTIONS(21),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 20,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_for,
    ACTIONS(29), 1,
      anon_sym_RBRACE,
    ACTIONS(31), 1,
      anon_sym_let,
    ACTIONS(33), 1,
      anon_sym_csg,
    ACTIONS(35), 1,
      anon_sym_if,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(45), 1,
      sym_identifier,
    ACTIONS(47), 1,
      sym_comment,
    STATE(20), 1,
      sym_gamma_expr,
    STATE(60), 1,
      sym_fn_call,
    STATE(89), 1,
      sym_expr,
    STATE(109), 1,
      sym_theta_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(62), 2,
      sym_stmt,
      aux_sym_block_repeat1,
    STATE(269), 3,
      sym_let,
      sym_assign,
      sym_csg,
    STATE(68), 12,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [77] = 20,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_for,
    ACTIONS(31), 1,
      anon_sym_let,
    ACTIONS(33), 1,
      anon_sym_csg,
    ACTIONS(35), 1,
      anon_sym_if,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(45), 1,
      sym_identifier,
    ACTIONS(49), 1,
      anon_sym_RBRACE,
    ACTIONS(51), 1,
      sym_comment,
    STATE(20), 1,
      sym_gamma_expr,
    STATE(60), 1,
      sym_fn_call,
    STATE(102), 1,
      sym_expr,
    STATE(109), 1,
      sym_theta_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(2), 2,
      sym_stmt,
      aux_sym_block_repeat1,
    STATE(269), 3,
      sym_let,
      sym_assign,
      sym_csg,
    STATE(68), 12,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [154] = 20,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_for,
    ACTIONS(31), 1,
      anon_sym_let,
    ACTIONS(33), 1,
      anon_sym_csg,
    ACTIONS(35), 1,
      anon_sym_if,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(45), 1,
      sym_identifier,
    ACTIONS(47), 1,
      sym_comment,
    ACTIONS(53), 1,
      anon_sym_RBRACE,
    STATE(20), 1,
      sym_gamma_expr,
    STATE(60), 1,
      sym_fn_call,
    STATE(92), 1,
      sym_expr,
    STATE(109), 1,
      sym_theta_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(62), 2,
      sym_stmt,
      aux_sym_block_repeat1,
    STATE(269), 3,
      sym_let,
      sym_assign,
      sym_csg,
    STATE(68), 12,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [231] = 20,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_for,
    ACTIONS(31), 1,
      anon_sym_let,
    ACTIONS(33), 1,
      anon_sym_csg,
    ACTIONS(35), 1,
      anon_sym_if,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(45), 1,
      sym_identifier,
    ACTIONS(55), 1,
      anon_sym_RBRACE,
    ACTIONS(57), 1,
      sym_comment,
    STATE(20), 1,
      sym_gamma_expr,
    STATE(60), 1,
      sym_fn_call,
    STATE(94), 1,
      sym_expr,
    STATE(109), 1,
      sym_theta_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(4), 2,
      sym_stmt,
      aux_sym_block_repeat1,
    STATE(269), 3,
      sym_let,
      sym_assign,
      sym_csg,
    STATE(68), 12,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [308] = 20,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_for,
    ACTIONS(31), 1,
      anon_sym_let,
    ACTIONS(33), 1,
      anon_sym_csg,
    ACTIONS(35), 1,
      anon_sym_if,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(45), 1,
      sym_identifier,
    ACTIONS(47), 1,
      sym_comment,
    ACTIONS(59), 1,
      anon_sym_RBRACE,
    STATE(20), 1,
      sym_gamma_expr,
    STATE(60), 1,
      sym_fn_call,
    STATE(95), 1,
      sym_expr,
    STATE(109), 1,
      sym_theta_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(62), 2,
      sym_stmt,
      aux_sym_block_repeat1,
    STATE(269), 3,
      sym_let,
      sym_assign,
      sym_csg,
    STATE(68), 12,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [385] = 20,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_for,
    ACTIONS(31), 1,
      anon_sym_let,
    ACTIONS(33), 1,
      anon_sym_csg,
    ACTIONS(35), 1,
      anon_sym_if,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(45), 1,
      sym_identifier,
    ACTIONS(61), 1,
      anon_sym_RBRACE,
    ACTIONS(63), 1,
      sym_comment,
    STATE(20), 1,
      sym_gamma_expr,
    STATE(60), 1,
      sym_fn_call,
    STATE(97), 1,
      sym_expr,
    STATE(109), 1,
      sym_theta_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(6), 2,
      sym_stmt,
      aux_sym_block_repeat1,
    STATE(269), 3,
      sym_let,
      sym_assign,
      sym_csg,
    STATE(68), 12,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [462] = 2,
    ACTIONS(67), 12,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(65), 21,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [500] = 2,
    ACTIONS(71), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(69), 28,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_RBRACK,
      anon_sym_module,
      anon_sym_SEMI,
      anon_sym_entity,
      anon_sym_RPAREN,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_else,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [536] = 2,
    ACTIONS(75), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(73), 28,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_RBRACK,
      anon_sym_module,
      anon_sym_SEMI,
      anon_sym_entity,
      anon_sym_RPAREN,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_else,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [572] = 2,
    ACTIONS(79), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(77), 28,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_RBRACK,
      anon_sym_module,
      anon_sym_SEMI,
      anon_sym_entity,
      anon_sym_RPAREN,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_else,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [608] = 2,
    ACTIONS(83), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(81), 28,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_RBRACK,
      anon_sym_module,
      anon_sym_SEMI,
      anon_sym_entity,
      anon_sym_RPAREN,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_else,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [644] = 14,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(85), 1,
      anon_sym_RPAREN,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(29), 1,
      aux_sym__param_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(113), 1,
      sym_expr,
    STATE(250), 1,
      sym__param_list,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [701] = 3,
    ACTIONS(95), 1,
      anon_sym_else,
    ACTIONS(93), 12,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(91), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [736] = 2,
    ACTIONS(79), 13,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_else,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(77), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [769] = 2,
    ACTIONS(83), 13,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_else,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(81), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [802] = 2,
    ACTIONS(71), 13,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_else,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(69), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [835] = 2,
    ACTIONS(75), 13,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_else,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(73), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [868] = 14,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    ACTIONS(97), 1,
      anon_sym_RPAREN,
    STATE(29), 1,
      aux_sym__param_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(113), 1,
      sym_expr,
    STATE(228), 1,
      sym__param_list,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [925] = 4,
    ACTIONS(101), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(99), 6,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      sym_digit,
      sym_comment,
    ACTIONS(103), 9,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(105), 9,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [961] = 2,
    ACTIONS(79), 12,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(77), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [993] = 2,
    ACTIONS(83), 12,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(81), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [1025] = 2,
    ACTIONS(71), 12,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(69), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [1057] = 2,
    ACTIONS(75), 12,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_BANG,
      anon_sym_SLASH,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
    ACTIONS(73), 15,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_digit,
      sym_comment,
  [1089] = 12,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(26), 1,
      aux_sym_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(105), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1140] = 12,
    ACTIONS(107), 1,
      anon_sym_LBRACK,
    ACTIONS(110), 1,
      anon_sym_LPAREN,
    ACTIONS(113), 1,
      anon_sym_if,
    ACTIONS(116), 1,
      anon_sym_eval,
    ACTIONS(125), 1,
      sym_digit,
    ACTIONS(128), 1,
      sym_identifier,
    STATE(26), 1,
      aux_sym_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(121), 1,
      sym_expr,
    ACTIONS(119), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(122), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1191] = 12,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(26), 1,
      aux_sym_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(115), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1242] = 12,
    ACTIONS(131), 1,
      anon_sym_LBRACK,
    ACTIONS(134), 1,
      anon_sym_LPAREN,
    ACTIONS(137), 1,
      anon_sym_if,
    ACTIONS(140), 1,
      anon_sym_eval,
    ACTIONS(149), 1,
      sym_digit,
    ACTIONS(152), 1,
      sym_identifier,
    STATE(28), 1,
      aux_sym__param_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(132), 1,
      sym_expr,
    ACTIONS(143), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(146), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1293] = 12,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(28), 1,
      aux_sym__param_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(119), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1344] = 12,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(27), 1,
      aux_sym_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(100), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1395] = 12,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(25), 1,
      aux_sym_list_repeat1,
    STATE(60), 1,
      sym_fn_call,
    STATE(111), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1446] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(81), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1494] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(90), 1,
      sym_fn_call,
    STATE(123), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1542] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(159), 1,
      sym_digit,
    ACTIONS(161), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(128), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(157), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1590] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(159), 1,
      sym_digit,
    ACTIONS(161), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(129), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(157), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1638] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(159), 1,
      sym_digit,
    ACTIONS(161), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(69), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(157), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1686] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(82), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1734] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(90), 1,
      sym_fn_call,
    STATE(130), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1782] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(122), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1830] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(159), 1,
      sym_digit,
    ACTIONS(161), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(124), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(157), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1878] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(90), 1,
      sym_fn_call,
    STATE(131), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1926] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(65), 1,
      sym_expr,
    STATE(90), 1,
      sym_fn_call,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [1974] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(90), 1,
      sym_fn_call,
    STATE(133), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2022] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(90), 1,
      sym_fn_call,
    STATE(117), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2070] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(90), 1,
      sym_fn_call,
    STATE(116), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2118] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(159), 1,
      sym_digit,
    ACTIONS(161), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(127), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(157), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2166] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(125), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2214] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(126), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2262] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(90), 1,
      sym_fn_call,
    STATE(112), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2310] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(65), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2358] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(69), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2406] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(83), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2454] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(159), 1,
      sym_digit,
    ACTIONS(161), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(120), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(157), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2502] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(69), 1,
      sym_expr,
    STATE(90), 1,
      sym_fn_call,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(155), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2550] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(159), 1,
      sym_digit,
    ACTIONS(161), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(65), 1,
      sym_expr,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    ACTIONS(157), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2598] = 11,
    ACTIONS(23), 1,
      anon_sym_LBRACK,
    ACTIONS(25), 1,
      anon_sym_LPAREN,
    ACTIONS(37), 1,
      anon_sym_eval,
    ACTIONS(43), 1,
      sym_digit,
    ACTIONS(87), 1,
      anon_sym_if,
    ACTIONS(89), 1,
      sym_identifier,
    STATE(60), 1,
      sym_fn_call,
    STATE(85), 1,
      sym_expr,
    ACTIONS(39), 2,
      anon_sym_DASH,
      anon_sym_BANG,
    ACTIONS(41), 2,
      anon_sym_true,
      anon_sym_false,
    STATE(68), 13,
      sym_gamma_expr,
      sym_eval_expr,
      sym_unary_expr,
      sym_field_access,
      sym_splat_expr,
      sym_binary_expr,
      sym_scope_call,
      sym_list,
      sym_tuple,
      sym__literal,
      sym_float_literal,
      sym_bool_literal,
      sym_int_literal,
  [2646] = 4,
    ACTIONS(167), 1,
      anon_sym_LBRACE,
    STATE(57), 2,
      sym_block,
      aux_sym_scope_call_repeat1,
    ACTIONS(165), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(163), 17,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2678] = 4,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(57), 2,
      sym_block,
      aux_sym_scope_call_repeat1,
    ACTIONS(172), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(170), 17,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2710] = 5,
    ACTIONS(176), 1,
      anon_sym_LPAREN,
    ACTIONS(178), 1,
      anon_sym_DOT,
    STATE(205), 1,
      aux_sym_field_access_repeat1,
    ACTIONS(101), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(105), 17,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2744] = 4,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(58), 2,
      sym_block,
      aux_sym_scope_call_repeat1,
    ACTIONS(101), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(105), 17,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2776] = 3,
    ACTIONS(184), 1,
      sym_digit,
    ACTIONS(182), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(180), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2805] = 12,
    ACTIONS(188), 1,
      anon_sym_for,
    ACTIONS(191), 1,
      anon_sym_let,
    ACTIONS(194), 1,
      anon_sym_csg,
    ACTIONS(197), 1,
      anon_sym_if,
    ACTIONS(202), 1,
      sym_identifier,
    ACTIONS(205), 1,
      sym_comment,
    STATE(109), 1,
      sym_theta_expr,
    STATE(110), 1,
      sym_gamma_expr,
    STATE(62), 2,
      sym_stmt,
      aux_sym_block_repeat1,
    ACTIONS(200), 3,
      anon_sym_eval,
      anon_sym_true,
      anon_sym_false,
    STATE(269), 3,
      sym_let,
      sym_assign,
      sym_csg,
    ACTIONS(186), 6,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_BANG,
      sym_digit,
  [2852] = 3,
    ACTIONS(208), 1,
      anon_sym_else,
    ACTIONS(93), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(91), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2881] = 2,
    ACTIONS(212), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(210), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2907] = 2,
    ACTIONS(216), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(214), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2933] = 2,
    ACTIONS(220), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(218), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2959] = 2,
    ACTIONS(224), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(222), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [2985] = 2,
    ACTIONS(101), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(105), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3011] = 2,
    ACTIONS(228), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(226), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3037] = 2,
    ACTIONS(232), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(230), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3063] = 3,
    ACTIONS(238), 1,
      anon_sym_DOT,
    ACTIONS(236), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(234), 17,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3091] = 3,
    ACTIONS(244), 1,
      anon_sym_DOT,
    ACTIONS(242), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(240), 17,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3119] = 2,
    ACTIONS(248), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(246), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3145] = 2,
    ACTIONS(252), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(250), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3171] = 2,
    ACTIONS(256), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(254), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3197] = 3,
    ACTIONS(244), 1,
      anon_sym_DOT,
    ACTIONS(260), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(258), 17,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3225] = 2,
    ACTIONS(264), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(262), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3251] = 2,
    ACTIONS(268), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(266), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3277] = 2,
    ACTIONS(272), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(270), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3303] = 2,
    ACTIONS(276), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(274), 18,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3329] = 7,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
    ACTIONS(214), 7,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3363] = 4,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(216), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(214), 15,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3391] = 6,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
    ACTIONS(214), 8,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3423] = 6,
    ACTIONS(176), 1,
      anon_sym_LPAREN,
    ACTIONS(178), 1,
      anon_sym_DOT,
    ACTIONS(290), 1,
      anon_sym_EQ,
    STATE(205), 1,
      aux_sym_field_access_repeat1,
    ACTIONS(101), 3,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_SLASH,
    ACTIONS(105), 12,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3455] = 5,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(216), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(214), 12,
      anon_sym_RBRACK,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      sym_comment,
  [3485] = 11,
    ACTIONS(292), 1,
      ts_builtin_sym_end,
    ACTIONS(294), 1,
      anon_sym_POUND,
    ACTIONS(297), 1,
      anon_sym_module,
    ACTIONS(300), 1,
      anon_sym_entity,
    ACTIONS(303), 1,
      anon_sym_concept,
    ACTIONS(306), 1,
      anon_sym_operation,
    ACTIONS(309), 1,
      anon_sym_impl,
    ACTIONS(312), 1,
      anon_sym_export,
    ACTIONS(315), 1,
      anon_sym_fn,
    ACTIONS(318), 1,
      sym_comment,
    STATE(86), 8,
      sym_ct_attrib,
      sym_module,
      sym_def_entity,
      sym_def_concept,
      sym_def_operation,
      sym_impl_block,
      sym_fn,
      aux_sym_source_file_repeat1,
  [3526] = 11,
    ACTIONS(5), 1,
      anon_sym_POUND,
    ACTIONS(7), 1,
      anon_sym_module,
    ACTIONS(9), 1,
      anon_sym_entity,
    ACTIONS(11), 1,
      anon_sym_concept,
    ACTIONS(13), 1,
      anon_sym_operation,
    ACTIONS(15), 1,
      anon_sym_impl,
    ACTIONS(17), 1,
      anon_sym_export,
    ACTIONS(19), 1,
      anon_sym_fn,
    ACTIONS(321), 1,
      ts_builtin_sym_end,
    ACTIONS(323), 1,
      sym_comment,
    STATE(86), 8,
      sym_ct_attrib,
      sym_module,
      sym_def_entity,
      sym_def_concept,
      sym_def_operation,
      sym_impl_block,
      sym_fn,
      aux_sym_source_file_repeat1,
  [3567] = 5,
    ACTIONS(176), 1,
      anon_sym_LPAREN,
    ACTIONS(325), 1,
      anon_sym_DOT,
    STATE(212), 1,
      aux_sym_field_access_repeat1,
    ACTIONS(101), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(105), 12,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_SLASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [3595] = 10,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(327), 1,
      anon_sym_RBRACE,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(331), 1,
      sym_comment,
    STATE(187), 1,
      aux_sym_block_repeat2,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [3632] = 4,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    ACTIONS(101), 2,
      anon_sym_LT,
      anon_sym_GT,
    STATE(104), 2,
      sym_block,
      aux_sym_scope_call_repeat1,
    ACTIONS(105), 11,
      anon_sym_DASH,
      anon_sym_SLASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [3657] = 12,
    ACTIONS(333), 1,
      anon_sym_LPAREN,
    ACTIONS(335), 1,
      anon_sym_csg,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(167), 1,
      sym__shaped_type,
    STATE(222), 1,
      sym_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [3698] = 10,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(349), 1,
      anon_sym_RBRACE,
    ACTIONS(351), 1,
      sym_comment,
    STATE(184), 1,
      aux_sym_block_repeat2,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [3735] = 12,
    ACTIONS(333), 1,
      anon_sym_LPAREN,
    ACTIONS(335), 1,
      anon_sym_csg,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(167), 1,
      sym__shaped_type,
    STATE(197), 1,
      sym_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [3776] = 10,
    ACTIONS(53), 1,
      anon_sym_RBRACE,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(353), 1,
      sym_comment,
    STATE(180), 1,
      aux_sym_block_repeat2,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [3813] = 10,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(355), 1,
      anon_sym_RBRACE,
    ACTIONS(357), 1,
      sym_comment,
    STATE(179), 1,
      aux_sym_block_repeat2,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [3850] = 12,
    ACTIONS(333), 1,
      anon_sym_LPAREN,
    ACTIONS(335), 1,
      anon_sym_csg,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(167), 1,
      sym__shaped_type,
    STATE(284), 1,
      sym_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [3891] = 10,
    ACTIONS(59), 1,
      anon_sym_RBRACE,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(359), 1,
      sym_comment,
    STATE(177), 1,
      aux_sym_block_repeat2,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [3928] = 12,
    ACTIONS(333), 1,
      anon_sym_LPAREN,
    ACTIONS(335), 1,
      anon_sym_csg,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(167), 1,
      sym__shaped_type,
    STATE(209), 1,
      sym_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [3969] = 12,
    ACTIONS(333), 1,
      anon_sym_LPAREN,
    ACTIONS(335), 1,
      anon_sym_csg,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(167), 1,
      sym__shaped_type,
    STATE(288), 1,
      sym_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [4010] = 9,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(361), 1,
      anon_sym_RBRACK,
    ACTIONS(363), 1,
      anon_sym_SEMI,
    ACTIONS(365), 1,
      anon_sym_COMMA,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4045] = 12,
    ACTIONS(333), 1,
      anon_sym_LPAREN,
    ACTIONS(335), 1,
      anon_sym_csg,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(167), 1,
      sym__shaped_type,
    STATE(193), 1,
      sym_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [4086] = 10,
    ACTIONS(29), 1,
      anon_sym_RBRACE,
    ACTIONS(282), 1,
      anon_sym_SLASH,
    ACTIONS(284), 1,
      anon_sym_STAR,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(367), 1,
      sym_comment,
    STATE(189), 1,
      aux_sym_block_repeat2,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4123] = 12,
    ACTIONS(333), 1,
      anon_sym_LPAREN,
    ACTIONS(335), 1,
      anon_sym_csg,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(167), 1,
      sym__shaped_type,
    STATE(218), 1,
      sym_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [4164] = 3,
    ACTIONS(172), 2,
      anon_sym_LT,
      anon_sym_GT,
    STATE(57), 2,
      sym_block,
      aux_sym_scope_call_repeat1,
    ACTIONS(170), 12,
      anon_sym_LBRACE,
      anon_sym_DASH,
      anon_sym_SLASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4187] = 8,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(365), 1,
      anon_sym_COMMA,
    ACTIONS(369), 1,
      anon_sym_RPAREN,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4219] = 3,
    ACTIONS(371), 1,
      anon_sym_DOT,
    ACTIONS(260), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(258), 12,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_SLASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4241] = 2,
    ACTIONS(373), 7,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_BANG,
      sym_digit,
      sym_comment,
    ACTIONS(375), 8,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
  [4261] = 3,
    ACTIONS(377), 1,
      anon_sym_DOT,
    ACTIONS(236), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(234), 12,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_SLASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4283] = 2,
    ACTIONS(99), 7,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_BANG,
      sym_digit,
      sym_comment,
    ACTIONS(103), 8,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
  [4303] = 2,
    ACTIONS(99), 7,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_BANG,
      sym_digit,
      sym_comment,
    ACTIONS(103), 8,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
  [4323] = 8,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(365), 1,
      anon_sym_COMMA,
    ACTIONS(379), 1,
      anon_sym_RPAREN,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4355] = 8,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    ACTIONS(389), 1,
      anon_sym_AMP_AMP,
    ACTIONS(391), 1,
      anon_sym_PIPE_PIPE,
    STATE(63), 1,
      sym_block,
    ACTIONS(381), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(385), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(383), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(387), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4387] = 8,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(393), 1,
      anon_sym_RPAREN,
    ACTIONS(395), 1,
      anon_sym_COMMA,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4419] = 3,
    ACTIONS(371), 1,
      anon_sym_DOT,
    ACTIONS(242), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(240), 12,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_SLASH,
      anon_sym_STAR,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4441] = 8,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(365), 1,
      anon_sym_COMMA,
    ACTIONS(397), 1,
      anon_sym_RBRACK,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4473] = 8,
    ACTIONS(389), 1,
      anon_sym_AMP_AMP,
    ACTIONS(391), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(399), 1,
      anon_sym_LBRACE,
    STATE(14), 1,
      sym_block,
    ACTIONS(381), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(385), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(383), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(387), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4505] = 8,
    ACTIONS(389), 1,
      anon_sym_AMP_AMP,
    ACTIONS(391), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(401), 1,
      anon_sym_LBRACE,
    STATE(118), 1,
      sym_block,
    ACTIONS(381), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(385), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(383), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(387), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4537] = 2,
    ACTIONS(403), 7,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_RBRACE,
      anon_sym_DASH,
      anon_sym_BANG,
      sym_digit,
      sym_comment,
    ACTIONS(405), 8,
      anon_sym_for,
      anon_sym_let,
      anon_sym_csg,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
  [4557] = 8,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(395), 1,
      anon_sym_COMMA,
    ACTIONS(407), 1,
      anon_sym_RPAREN,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4589] = 3,
    ACTIONS(216), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(409), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(214), 10,
      anon_sym_DOT_DOT,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4610] = 7,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(365), 1,
      anon_sym_COMMA,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4639] = 7,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(411), 1,
      anon_sym_SEMI,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4668] = 6,
    ACTIONS(389), 1,
      anon_sym_AMP_AMP,
    ACTIONS(214), 2,
      anon_sym_LBRACE,
      anon_sym_PIPE_PIPE,
    ACTIONS(381), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(385), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(383), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(387), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4695] = 7,
    ACTIONS(415), 1,
      anon_sym_DOT_DOT,
    ACTIONS(421), 1,
      anon_sym_AMP_AMP,
    ACTIONS(423), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(409), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(413), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(417), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(419), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4724] = 7,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(425), 1,
      anon_sym_SEMI,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4753] = 7,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(427), 1,
      anon_sym_SEMI,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4782] = 4,
    ACTIONS(216), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(409), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(417), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(214), 7,
      anon_sym_DOT_DOT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4805] = 5,
    ACTIONS(409), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(413), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(214), 3,
      anon_sym_DOT_DOT,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
    ACTIONS(417), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(419), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4830] = 6,
    ACTIONS(421), 1,
      anon_sym_AMP_AMP,
    ACTIONS(214), 2,
      anon_sym_DOT_DOT,
      anon_sym_PIPE_PIPE,
    ACTIONS(409), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(413), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(417), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(419), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4857] = 4,
    ACTIONS(216), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(385), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(383), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(214), 7,
      anon_sym_LBRACE,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4880] = 3,
    ACTIONS(216), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(385), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(214), 10,
      anon_sym_LBRACE,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
  [4901] = 7,
    ACTIONS(288), 1,
      anon_sym_AMP_AMP,
    ACTIONS(329), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(395), 1,
      anon_sym_COMMA,
    ACTIONS(278), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(284), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(280), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(286), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4930] = 5,
    ACTIONS(381), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(385), 2,
      anon_sym_SLASH,
      anon_sym_STAR,
    ACTIONS(214), 3,
      anon_sym_LBRACE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
    ACTIONS(383), 3,
      anon_sym_DASH,
      anon_sym_PLUS,
      anon_sym_PERCENT,
    ACTIONS(387), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [4955] = 9,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(208), 1,
      sym__shaped_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [4987] = 9,
    ACTIONS(337), 1,
      anon_sym_interval,
    ACTIONS(339), 1,
      anon_sym_vec,
    ACTIONS(341), 1,
      anon_sym_mat,
    ACTIONS(343), 1,
      anon_sym_tensor,
    ACTIONS(347), 1,
      anon_sym_int,
    STATE(157), 1,
      sym_shape,
    STATE(164), 1,
      sym_data_type,
    STATE(195), 1,
      sym__shaped_type,
    ACTIONS(345), 5,
      anon_sym_real,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [5019] = 1,
    ACTIONS(429), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5032] = 1,
    ACTIONS(431), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5045] = 1,
    ACTIONS(433), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5058] = 2,
    ACTIONS(435), 5,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_DASH,
      anon_sym_BANG,
      sym_digit,
    ACTIONS(437), 5,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
  [5073] = 1,
    ACTIONS(439), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5086] = 1,
    ACTIONS(441), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5099] = 1,
    ACTIONS(443), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5112] = 1,
    ACTIONS(445), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5125] = 1,
    ACTIONS(447), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5138] = 1,
    ACTIONS(449), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5151] = 1,
    ACTIONS(451), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5164] = 1,
    ACTIONS(453), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5177] = 1,
    ACTIONS(455), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5190] = 1,
    ACTIONS(457), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5203] = 2,
    ACTIONS(459), 5,
      anon_sym_LBRACK,
      anon_sym_LPAREN,
      anon_sym_DASH,
      anon_sym_BANG,
      sym_digit,
    ACTIONS(461), 5,
      anon_sym_if,
      anon_sym_eval,
      anon_sym_true,
      anon_sym_false,
      sym_identifier,
  [5218] = 1,
    ACTIONS(463), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5231] = 1,
    ACTIONS(465), 10,
      ts_builtin_sym_end,
      anon_sym_POUND,
      anon_sym_module,
      anon_sym_entity,
      anon_sym_concept,
      anon_sym_operation,
      anon_sym_impl,
      anon_sym_export,
      anon_sym_fn,
      sym_comment,
  [5244] = 2,
    STATE(287), 1,
      sym_data_type,
    ACTIONS(345), 6,
      anon_sym_real,
      anon_sym_int,
      anon_sym_complex,
      anon_sym_quat,
      anon_sym_bool,
      anon_sym_none,
  [5256] = 2,
    ACTIONS(469), 1,
      anon_sym_x,
    ACTIONS(467), 6,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_LT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5268] = 1,
    ACTIONS(471), 6,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_LT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5277] = 1,
    ACTIONS(473), 6,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_LT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5286] = 2,
    ACTIONS(477), 1,
      anon_sym_LT,
    ACTIONS(475), 5,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5297] = 1,
    ACTIONS(479), 6,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_LT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5306] = 1,
    ACTIONS(481), 6,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_GT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5315] = 1,
    ACTIONS(467), 6,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_LT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5324] = 1,
    ACTIONS(483), 5,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5332] = 1,
    ACTIONS(485), 5,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5340] = 5,
    ACTIONS(487), 1,
      anon_sym_RPAREN,
    ACTIONS(489), 1,
      sym_identifier,
    STATE(171), 1,
      aux_sym__arg_list_repeat1,
    STATE(210), 1,
      sym_typed_arg,
    STATE(266), 1,
      sym__arg_list,
  [5356] = 1,
    ACTIONS(491), 5,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5364] = 5,
    ACTIONS(489), 1,
      sym_identifier,
    ACTIONS(493), 1,
      anon_sym_RPAREN,
    STATE(171), 1,
      aux_sym__arg_list_repeat1,
    STATE(210), 1,
      sym_typed_arg,
    STATE(252), 1,
      sym__arg_list,
  [5380] = 5,
    ACTIONS(489), 1,
      sym_identifier,
    ACTIONS(495), 1,
      anon_sym_RPAREN,
    STATE(171), 1,
      aux_sym__arg_list_repeat1,
    STATE(210), 1,
      sym_typed_arg,
    STATE(244), 1,
      sym__arg_list,
  [5396] = 1,
    ACTIONS(497), 5,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5404] = 1,
    ACTIONS(499), 5,
      anon_sym_SEMI,
      anon_sym_RPAREN,
      anon_sym_DASH_GT,
      anon_sym_COMMA,
      anon_sym_LBRACE,
  [5412] = 5,
    ACTIONS(489), 1,
      sym_identifier,
    ACTIONS(501), 1,
      anon_sym_RPAREN,
    STATE(171), 1,
      aux_sym__arg_list_repeat1,
    STATE(210), 1,
      sym_typed_arg,
    STATE(274), 1,
      sym__arg_list,
  [5428] = 2,
    STATE(114), 1,
      sym__field_accessor,
    ACTIONS(503), 2,
      sym_digit,
      sym_identifier,
  [5436] = 3,
    ACTIONS(489), 1,
      sym_identifier,
    STATE(186), 1,
      aux_sym__arg_list_repeat1,
    STATE(192), 1,
      sym_typed_arg,
  [5446] = 3,
    ACTIONS(505), 1,
      anon_sym_RPAREN,
    ACTIONS(507), 1,
      anon_sym_COMMA,
    STATE(172), 1,
      aux_sym_type_repeat1,
  [5456] = 3,
    ACTIONS(510), 1,
      sym_identifier,
    STATE(207), 1,
      aux_sym__ident_list_repeat1,
    STATE(270), 1,
      sym__ident_list,
  [5466] = 3,
    ACTIONS(512), 1,
      anon_sym_GT,
    ACTIONS(514), 1,
      anon_sym_COMMA,
    STATE(174), 1,
      aux_sym_shape_repeat1,
  [5476] = 3,
    ACTIONS(517), 1,
      anon_sym_GT,
    ACTIONS(519), 1,
      anon_sym_COMMA,
    STATE(174), 1,
      aux_sym_shape_repeat1,
  [5486] = 2,
    STATE(72), 1,
      sym__field_accessor,
    ACTIONS(521), 2,
      sym_digit,
      sym_identifier,
  [5494] = 3,
    ACTIONS(355), 1,
      anon_sym_RBRACE,
    ACTIONS(523), 1,
      sym_comment,
    STATE(178), 1,
      aux_sym_block_repeat2,
  [5504] = 3,
    ACTIONS(525), 1,
      anon_sym_RBRACE,
    ACTIONS(527), 1,
      sym_comment,
    STATE(178), 1,
      aux_sym_block_repeat2,
  [5514] = 3,
    ACTIONS(523), 1,
      sym_comment,
    ACTIONS(530), 1,
      anon_sym_RBRACE,
    STATE(178), 1,
      aux_sym_block_repeat2,
  [5524] = 3,
    ACTIONS(349), 1,
      anon_sym_RBRACE,
    ACTIONS(523), 1,
      sym_comment,
    STATE(178), 1,
      aux_sym_block_repeat2,
  [5534] = 2,
    STATE(76), 1,
      sym__field_accessor,
    ACTIONS(532), 2,
      sym_digit,
      sym_identifier,
  [5542] = 2,
    STATE(106), 1,
      sym__field_accessor,
    ACTIONS(534), 2,
      sym_digit,
      sym_identifier,
  [5550] = 3,
    ACTIONS(519), 1,
      anon_sym_COMMA,
    ACTIONS(536), 1,
      anon_sym_GT,
    STATE(175), 1,
      aux_sym_shape_repeat1,
  [5560] = 3,
    ACTIONS(523), 1,
      sym_comment,
    ACTIONS(538), 1,
      anon_sym_RBRACE,
    STATE(178), 1,
      aux_sym_block_repeat2,
  [5570] = 2,
    STATE(233), 1,
      sym__field_accessor,
    ACTIONS(540), 2,
      sym_digit,
      sym_identifier,
  [5578] = 3,
    ACTIONS(542), 1,
      sym_identifier,
    STATE(186), 1,
      aux_sym__arg_list_repeat1,
    STATE(230), 1,
      sym_typed_arg,
  [5588] = 3,
    ACTIONS(523), 1,
      sym_comment,
    ACTIONS(545), 1,
      anon_sym_RBRACE,
    STATE(178), 1,
      aux_sym_block_repeat2,
  [5598] = 3,
    ACTIONS(547), 1,
      anon_sym_RPAREN,
    ACTIONS(549), 1,
      anon_sym_COMMA,
    STATE(172), 1,
      aux_sym_type_repeat1,
  [5608] = 3,
    ACTIONS(327), 1,
      anon_sym_RBRACE,
    ACTIONS(523), 1,
      sym_comment,
    STATE(178), 1,
      aux_sym_block_repeat2,
  [5618] = 3,
    ACTIONS(551), 1,
      sym_identifier,
    STATE(191), 1,
      aux_sym__path_repeat1,
    STATE(267), 1,
      sym__path,
  [5628] = 2,
    ACTIONS(553), 1,
      sym_identifier,
    STATE(204), 1,
      aux_sym__path_repeat1,
  [5635] = 2,
    ACTIONS(555), 1,
      anon_sym_RPAREN,
    ACTIONS(557), 1,
      anon_sym_COMMA,
  [5642] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(149), 1,
      sym_block,
  [5649] = 2,
    ACTIONS(559), 1,
      anon_sym_LT,
    ACTIONS(561), 1,
      anon_sym_for,
  [5656] = 2,
    ACTIONS(549), 1,
      anon_sym_COMMA,
    STATE(188), 1,
      aux_sym_type_repeat1,
  [5663] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(151), 1,
      sym_block,
  [5670] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(146), 1,
      sym_block,
  [5677] = 2,
    ACTIONS(563), 1,
      anon_sym_RPAREN,
    ACTIONS(565), 1,
      sym_identifier,
  [5684] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(136), 1,
      sym_block,
  [5691] = 2,
    ACTIONS(567), 1,
      anon_sym_SEMI,
    ACTIONS(569), 1,
      anon_sym_COLON_COLON,
  [5698] = 2,
    ACTIONS(571), 1,
      sym_identifier,
    STATE(201), 1,
      aux_sym__ident_list_repeat1,
  [5705] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(8), 1,
      sym_block,
  [5712] = 1,
    ACTIONS(512), 2,
      anon_sym_GT,
      anon_sym_COMMA,
  [5717] = 2,
    ACTIONS(574), 1,
      sym_identifier,
    STATE(204), 1,
      aux_sym__path_repeat1,
  [5724] = 2,
    ACTIONS(577), 1,
      anon_sym_DOT,
    STATE(216), 1,
      aux_sym_field_access_repeat1,
  [5731] = 2,
    ACTIONS(579), 1,
      anon_sym_RPAREN,
    ACTIONS(581), 1,
      sym_identifier,
  [5738] = 2,
    ACTIONS(583), 1,
      sym_identifier,
    STATE(201), 1,
      aux_sym__ident_list_repeat1,
  [5745] = 1,
    ACTIONS(585), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [5750] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(138), 1,
      sym_block,
  [5757] = 2,
    ACTIONS(557), 1,
      anon_sym_COMMA,
    ACTIONS(587), 1,
      anon_sym_RPAREN,
  [5764] = 2,
    ACTIONS(589), 1,
      anon_sym_GT,
    ACTIONS(591), 1,
      anon_sym_COMMA,
  [5771] = 2,
    ACTIONS(593), 1,
      anon_sym_DOT,
    STATE(216), 1,
      aux_sym_field_access_repeat1,
  [5778] = 2,
    ACTIONS(569), 1,
      anon_sym_COLON_COLON,
    ACTIONS(595), 1,
      anon_sym_SEMI,
  [5785] = 2,
    ACTIONS(597), 1,
      sym_identifier,
    STATE(246), 1,
      sym_fn_call,
  [5792] = 2,
    ACTIONS(591), 1,
      anon_sym_COMMA,
    ACTIONS(599), 1,
      anon_sym_GT,
  [5799] = 2,
    ACTIONS(601), 1,
      anon_sym_DOT,
    STATE(216), 1,
      aux_sym_field_access_repeat1,
  [5806] = 2,
    ACTIONS(604), 1,
      sym_digit,
    STATE(255), 1,
      sym_int_literal,
  [5813] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(145), 1,
      sym_block,
  [5820] = 2,
    ACTIONS(401), 1,
      anon_sym_LBRACE,
    STATE(8), 1,
      sym_block,
  [5827] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(147), 1,
      sym_block,
  [5834] = 2,
    ACTIONS(174), 1,
      anon_sym_LBRACE,
    STATE(140), 1,
      sym_block,
  [5841] = 1,
    ACTIONS(606), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [5846] = 1,
    ACTIONS(608), 1,
      anon_sym_LPAREN,
  [5850] = 1,
    ACTIONS(610), 1,
      anon_sym_for,
  [5854] = 1,
    ACTIONS(612), 1,
      anon_sym_RPAREN,
  [5858] = 1,
    ACTIONS(614), 1,
      anon_sym_EQ,
  [5862] = 1,
    ACTIONS(616), 1,
      anon_sym_SEMI,
  [5866] = 1,
    ACTIONS(618), 1,
      anon_sym_RPAREN,
  [5870] = 1,
    ACTIONS(620), 1,
      sym_identifier,
  [5874] = 1,
    ACTIONS(557), 1,
      anon_sym_COMMA,
  [5878] = 1,
    ACTIONS(622), 1,
      sym_digit,
  [5882] = 1,
    ACTIONS(624), 1,
      sym_identifier,
  [5886] = 1,
    ACTIONS(244), 1,
      anon_sym_DOT,
  [5890] = 1,
    ACTIONS(569), 1,
      anon_sym_COLON_COLON,
  [5894] = 1,
    ACTIONS(626), 1,
      anon_sym_SEMI,
  [5898] = 1,
    ACTIONS(628), 1,
      anon_sym_COLON,
  [5902] = 1,
    ACTIONS(630), 1,
      sym_digit,
  [5906] = 1,
    ACTIONS(632), 1,
      anon_sym_EQ,
  [5910] = 1,
    ACTIONS(634), 1,
      anon_sym_EQ,
  [5914] = 1,
    ACTIONS(636), 1,
      anon_sym_in,
  [5918] = 1,
    ACTIONS(638), 1,
      sym_identifier,
  [5922] = 1,
    ACTIONS(640), 1,
      anon_sym_DASH_GT,
  [5926] = 1,
    ACTIONS(642), 1,
      anon_sym_LPAREN,
  [5930] = 1,
    ACTIONS(644), 1,
      anon_sym_RPAREN,
  [5934] = 1,
    ACTIONS(646), 1,
      sym_identifier,
  [5938] = 1,
    ACTIONS(648), 1,
      anon_sym_RBRACK,
  [5942] = 1,
    ACTIONS(650), 1,
      anon_sym_SEMI,
  [5946] = 1,
    ACTIONS(652), 1,
      anon_sym_DASH_GT,
  [5950] = 1,
    ACTIONS(654), 1,
      anon_sym_DOT,
  [5954] = 1,
    ACTIONS(656), 1,
      anon_sym_RPAREN,
  [5958] = 1,
    ACTIONS(658), 1,
      sym_identifier,
  [5962] = 1,
    ACTIONS(660), 1,
      anon_sym_RPAREN,
  [5966] = 1,
    ACTIONS(662), 1,
      anon_sym_DASH_GT,
  [5970] = 1,
    ACTIONS(176), 1,
      anon_sym_LPAREN,
  [5974] = 1,
    ACTIONS(664), 1,
      anon_sym_RBRACK,
  [5978] = 1,
    ACTIONS(666), 1,
      anon_sym_LPAREN,
  [5982] = 1,
    ACTIONS(668), 1,
      anon_sym_LPAREN,
  [5986] = 1,
    ACTIONS(670), 1,
      sym_identifier,
  [5990] = 1,
    ACTIONS(234), 1,
      anon_sym_RBRACK,
  [5994] = 1,
    ACTIONS(672), 1,
      anon_sym_LPAREN,
  [5998] = 1,
    ACTIONS(674), 1,
      anon_sym_COLON,
  [6002] = 1,
    ACTIONS(676), 1,
      anon_sym_LPAREN,
  [6006] = 1,
    ACTIONS(678), 1,
      sym_digit,
  [6010] = 1,
    ACTIONS(680), 1,
      anon_sym_LPAREN,
  [6014] = 1,
    ACTIONS(682), 1,
      sym_digit,
  [6018] = 1,
    ACTIONS(684), 1,
      anon_sym_RPAREN,
  [6022] = 1,
    ACTIONS(686), 1,
      anon_sym_SEMI,
  [6026] = 1,
    ACTIONS(688), 1,
      anon_sym_LBRACK,
  [6030] = 1,
    ACTIONS(690), 1,
      anon_sym_SEMI,
  [6034] = 1,
    ACTIONS(692), 1,
      anon_sym_GT,
  [6038] = 1,
    ACTIONS(694), 1,
      ts_builtin_sym_end,
  [6042] = 1,
    ACTIONS(696), 1,
      sym_identifier,
  [6046] = 1,
    ACTIONS(698), 1,
      sym_identifier,
  [6050] = 1,
    ACTIONS(700), 1,
      anon_sym_RPAREN,
  [6054] = 1,
    ACTIONS(702), 1,
      anon_sym_SEMI,
  [6058] = 1,
    ACTIONS(704), 1,
      sym_identifier,
  [6062] = 1,
    ACTIONS(706), 1,
      sym_identifier,
  [6066] = 1,
    ACTIONS(708), 1,
      anon_sym_fn,
  [6070] = 1,
    ACTIONS(710), 1,
      sym_identifier,
  [6074] = 1,
    ACTIONS(712), 1,
      anon_sym_DASH_GT,
  [6078] = 1,
    ACTIONS(714), 1,
      anon_sym_RPAREN,
  [6082] = 1,
    ACTIONS(591), 1,
      anon_sym_COMMA,
  [6086] = 1,
    ACTIONS(716), 1,
      sym_identifier,
  [6090] = 1,
    ACTIONS(718), 1,
      anon_sym_DASH_GT,
  [6094] = 1,
    ACTIONS(720), 1,
      sym_identifier,
  [6098] = 1,
    ACTIONS(722), 1,
      sym_identifier,
  [6102] = 1,
    ACTIONS(724), 1,
      anon_sym_GT,
  [6106] = 1,
    ACTIONS(726), 1,
      anon_sym_SEMI,
  [6110] = 1,
    ACTIONS(728), 1,
      sym_identifier,
  [6114] = 1,
    ACTIONS(730), 1,
      anon_sym_LT,
  [6118] = 1,
    ACTIONS(732), 1,
      sym_identifier,
  [6122] = 1,
    ACTIONS(536), 1,
      sym_digit,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 77,
  [SMALL_STATE(4)] = 154,
  [SMALL_STATE(5)] = 231,
  [SMALL_STATE(6)] = 308,
  [SMALL_STATE(7)] = 385,
  [SMALL_STATE(8)] = 462,
  [SMALL_STATE(9)] = 500,
  [SMALL_STATE(10)] = 536,
  [SMALL_STATE(11)] = 572,
  [SMALL_STATE(12)] = 608,
  [SMALL_STATE(13)] = 644,
  [SMALL_STATE(14)] = 701,
  [SMALL_STATE(15)] = 736,
  [SMALL_STATE(16)] = 769,
  [SMALL_STATE(17)] = 802,
  [SMALL_STATE(18)] = 835,
  [SMALL_STATE(19)] = 868,
  [SMALL_STATE(20)] = 925,
  [SMALL_STATE(21)] = 961,
  [SMALL_STATE(22)] = 993,
  [SMALL_STATE(23)] = 1025,
  [SMALL_STATE(24)] = 1057,
  [SMALL_STATE(25)] = 1089,
  [SMALL_STATE(26)] = 1140,
  [SMALL_STATE(27)] = 1191,
  [SMALL_STATE(28)] = 1242,
  [SMALL_STATE(29)] = 1293,
  [SMALL_STATE(30)] = 1344,
  [SMALL_STATE(31)] = 1395,
  [SMALL_STATE(32)] = 1446,
  [SMALL_STATE(33)] = 1494,
  [SMALL_STATE(34)] = 1542,
  [SMALL_STATE(35)] = 1590,
  [SMALL_STATE(36)] = 1638,
  [SMALL_STATE(37)] = 1686,
  [SMALL_STATE(38)] = 1734,
  [SMALL_STATE(39)] = 1782,
  [SMALL_STATE(40)] = 1830,
  [SMALL_STATE(41)] = 1878,
  [SMALL_STATE(42)] = 1926,
  [SMALL_STATE(43)] = 1974,
  [SMALL_STATE(44)] = 2022,
  [SMALL_STATE(45)] = 2070,
  [SMALL_STATE(46)] = 2118,
  [SMALL_STATE(47)] = 2166,
  [SMALL_STATE(48)] = 2214,
  [SMALL_STATE(49)] = 2262,
  [SMALL_STATE(50)] = 2310,
  [SMALL_STATE(51)] = 2358,
  [SMALL_STATE(52)] = 2406,
  [SMALL_STATE(53)] = 2454,
  [SMALL_STATE(54)] = 2502,
  [SMALL_STATE(55)] = 2550,
  [SMALL_STATE(56)] = 2598,
  [SMALL_STATE(57)] = 2646,
  [SMALL_STATE(58)] = 2678,
  [SMALL_STATE(59)] = 2710,
  [SMALL_STATE(60)] = 2744,
  [SMALL_STATE(61)] = 2776,
  [SMALL_STATE(62)] = 2805,
  [SMALL_STATE(63)] = 2852,
  [SMALL_STATE(64)] = 2881,
  [SMALL_STATE(65)] = 2907,
  [SMALL_STATE(66)] = 2933,
  [SMALL_STATE(67)] = 2959,
  [SMALL_STATE(68)] = 2985,
  [SMALL_STATE(69)] = 3011,
  [SMALL_STATE(70)] = 3037,
  [SMALL_STATE(71)] = 3063,
  [SMALL_STATE(72)] = 3091,
  [SMALL_STATE(73)] = 3119,
  [SMALL_STATE(74)] = 3145,
  [SMALL_STATE(75)] = 3171,
  [SMALL_STATE(76)] = 3197,
  [SMALL_STATE(77)] = 3225,
  [SMALL_STATE(78)] = 3251,
  [SMALL_STATE(79)] = 3277,
  [SMALL_STATE(80)] = 3303,
  [SMALL_STATE(81)] = 3329,
  [SMALL_STATE(82)] = 3363,
  [SMALL_STATE(83)] = 3391,
  [SMALL_STATE(84)] = 3423,
  [SMALL_STATE(85)] = 3455,
  [SMALL_STATE(86)] = 3485,
  [SMALL_STATE(87)] = 3526,
  [SMALL_STATE(88)] = 3567,
  [SMALL_STATE(89)] = 3595,
  [SMALL_STATE(90)] = 3632,
  [SMALL_STATE(91)] = 3657,
  [SMALL_STATE(92)] = 3698,
  [SMALL_STATE(93)] = 3735,
  [SMALL_STATE(94)] = 3776,
  [SMALL_STATE(95)] = 3813,
  [SMALL_STATE(96)] = 3850,
  [SMALL_STATE(97)] = 3891,
  [SMALL_STATE(98)] = 3928,
  [SMALL_STATE(99)] = 3969,
  [SMALL_STATE(100)] = 4010,
  [SMALL_STATE(101)] = 4045,
  [SMALL_STATE(102)] = 4086,
  [SMALL_STATE(103)] = 4123,
  [SMALL_STATE(104)] = 4164,
  [SMALL_STATE(105)] = 4187,
  [SMALL_STATE(106)] = 4219,
  [SMALL_STATE(107)] = 4241,
  [SMALL_STATE(108)] = 4261,
  [SMALL_STATE(109)] = 4283,
  [SMALL_STATE(110)] = 4303,
  [SMALL_STATE(111)] = 4323,
  [SMALL_STATE(112)] = 4355,
  [SMALL_STATE(113)] = 4387,
  [SMALL_STATE(114)] = 4419,
  [SMALL_STATE(115)] = 4441,
  [SMALL_STATE(116)] = 4473,
  [SMALL_STATE(117)] = 4505,
  [SMALL_STATE(118)] = 4537,
  [SMALL_STATE(119)] = 4557,
  [SMALL_STATE(120)] = 4589,
  [SMALL_STATE(121)] = 4610,
  [SMALL_STATE(122)] = 4639,
  [SMALL_STATE(123)] = 4668,
  [SMALL_STATE(124)] = 4695,
  [SMALL_STATE(125)] = 4724,
  [SMALL_STATE(126)] = 4753,
  [SMALL_STATE(127)] = 4782,
  [SMALL_STATE(128)] = 4805,
  [SMALL_STATE(129)] = 4830,
  [SMALL_STATE(130)] = 4857,
  [SMALL_STATE(131)] = 4880,
  [SMALL_STATE(132)] = 4901,
  [SMALL_STATE(133)] = 4930,
  [SMALL_STATE(134)] = 4955,
  [SMALL_STATE(135)] = 4987,
  [SMALL_STATE(136)] = 5019,
  [SMALL_STATE(137)] = 5032,
  [SMALL_STATE(138)] = 5045,
  [SMALL_STATE(139)] = 5058,
  [SMALL_STATE(140)] = 5073,
  [SMALL_STATE(141)] = 5086,
  [SMALL_STATE(142)] = 5099,
  [SMALL_STATE(143)] = 5112,
  [SMALL_STATE(144)] = 5125,
  [SMALL_STATE(145)] = 5138,
  [SMALL_STATE(146)] = 5151,
  [SMALL_STATE(147)] = 5164,
  [SMALL_STATE(148)] = 5177,
  [SMALL_STATE(149)] = 5190,
  [SMALL_STATE(150)] = 5203,
  [SMALL_STATE(151)] = 5218,
  [SMALL_STATE(152)] = 5231,
  [SMALL_STATE(153)] = 5244,
  [SMALL_STATE(154)] = 5256,
  [SMALL_STATE(155)] = 5268,
  [SMALL_STATE(156)] = 5277,
  [SMALL_STATE(157)] = 5286,
  [SMALL_STATE(158)] = 5297,
  [SMALL_STATE(159)] = 5306,
  [SMALL_STATE(160)] = 5315,
  [SMALL_STATE(161)] = 5324,
  [SMALL_STATE(162)] = 5332,
  [SMALL_STATE(163)] = 5340,
  [SMALL_STATE(164)] = 5356,
  [SMALL_STATE(165)] = 5364,
  [SMALL_STATE(166)] = 5380,
  [SMALL_STATE(167)] = 5396,
  [SMALL_STATE(168)] = 5404,
  [SMALL_STATE(169)] = 5412,
  [SMALL_STATE(170)] = 5428,
  [SMALL_STATE(171)] = 5436,
  [SMALL_STATE(172)] = 5446,
  [SMALL_STATE(173)] = 5456,
  [SMALL_STATE(174)] = 5466,
  [SMALL_STATE(175)] = 5476,
  [SMALL_STATE(176)] = 5486,
  [SMALL_STATE(177)] = 5494,
  [SMALL_STATE(178)] = 5504,
  [SMALL_STATE(179)] = 5514,
  [SMALL_STATE(180)] = 5524,
  [SMALL_STATE(181)] = 5534,
  [SMALL_STATE(182)] = 5542,
  [SMALL_STATE(183)] = 5550,
  [SMALL_STATE(184)] = 5560,
  [SMALL_STATE(185)] = 5570,
  [SMALL_STATE(186)] = 5578,
  [SMALL_STATE(187)] = 5588,
  [SMALL_STATE(188)] = 5598,
  [SMALL_STATE(189)] = 5608,
  [SMALL_STATE(190)] = 5618,
  [SMALL_STATE(191)] = 5628,
  [SMALL_STATE(192)] = 5635,
  [SMALL_STATE(193)] = 5642,
  [SMALL_STATE(194)] = 5649,
  [SMALL_STATE(195)] = 5656,
  [SMALL_STATE(196)] = 5663,
  [SMALL_STATE(197)] = 5670,
  [SMALL_STATE(198)] = 5677,
  [SMALL_STATE(199)] = 5684,
  [SMALL_STATE(200)] = 5691,
  [SMALL_STATE(201)] = 5698,
  [SMALL_STATE(202)] = 5705,
  [SMALL_STATE(203)] = 5712,
  [SMALL_STATE(204)] = 5717,
  [SMALL_STATE(205)] = 5724,
  [SMALL_STATE(206)] = 5731,
  [SMALL_STATE(207)] = 5738,
  [SMALL_STATE(208)] = 5745,
  [SMALL_STATE(209)] = 5750,
  [SMALL_STATE(210)] = 5757,
  [SMALL_STATE(211)] = 5764,
  [SMALL_STATE(212)] = 5771,
  [SMALL_STATE(213)] = 5778,
  [SMALL_STATE(214)] = 5785,
  [SMALL_STATE(215)] = 5792,
  [SMALL_STATE(216)] = 5799,
  [SMALL_STATE(217)] = 5806,
  [SMALL_STATE(218)] = 5813,
  [SMALL_STATE(219)] = 5820,
  [SMALL_STATE(220)] = 5827,
  [SMALL_STATE(221)] = 5834,
  [SMALL_STATE(222)] = 5841,
  [SMALL_STATE(223)] = 5846,
  [SMALL_STATE(224)] = 5850,
  [SMALL_STATE(225)] = 5854,
  [SMALL_STATE(226)] = 5858,
  [SMALL_STATE(227)] = 5862,
  [SMALL_STATE(228)] = 5866,
  [SMALL_STATE(229)] = 5870,
  [SMALL_STATE(230)] = 5874,
  [SMALL_STATE(231)] = 5878,
  [SMALL_STATE(232)] = 5882,
  [SMALL_STATE(233)] = 5886,
  [SMALL_STATE(234)] = 5890,
  [SMALL_STATE(235)] = 5894,
  [SMALL_STATE(236)] = 5898,
  [SMALL_STATE(237)] = 5902,
  [SMALL_STATE(238)] = 5906,
  [SMALL_STATE(239)] = 5910,
  [SMALL_STATE(240)] = 5914,
  [SMALL_STATE(241)] = 5918,
  [SMALL_STATE(242)] = 5922,
  [SMALL_STATE(243)] = 5926,
  [SMALL_STATE(244)] = 5930,
  [SMALL_STATE(245)] = 5934,
  [SMALL_STATE(246)] = 5938,
  [SMALL_STATE(247)] = 5942,
  [SMALL_STATE(248)] = 5946,
  [SMALL_STATE(249)] = 5950,
  [SMALL_STATE(250)] = 5954,
  [SMALL_STATE(251)] = 5958,
  [SMALL_STATE(252)] = 5962,
  [SMALL_STATE(253)] = 5966,
  [SMALL_STATE(254)] = 5970,
  [SMALL_STATE(255)] = 5974,
  [SMALL_STATE(256)] = 5978,
  [SMALL_STATE(257)] = 5982,
  [SMALL_STATE(258)] = 5986,
  [SMALL_STATE(259)] = 5990,
  [SMALL_STATE(260)] = 5994,
  [SMALL_STATE(261)] = 5998,
  [SMALL_STATE(262)] = 6002,
  [SMALL_STATE(263)] = 6006,
  [SMALL_STATE(264)] = 6010,
  [SMALL_STATE(265)] = 6014,
  [SMALL_STATE(266)] = 6018,
  [SMALL_STATE(267)] = 6022,
  [SMALL_STATE(268)] = 6026,
  [SMALL_STATE(269)] = 6030,
  [SMALL_STATE(270)] = 6034,
  [SMALL_STATE(271)] = 6038,
  [SMALL_STATE(272)] = 6042,
  [SMALL_STATE(273)] = 6046,
  [SMALL_STATE(274)] = 6050,
  [SMALL_STATE(275)] = 6054,
  [SMALL_STATE(276)] = 6058,
  [SMALL_STATE(277)] = 6062,
  [SMALL_STATE(278)] = 6066,
  [SMALL_STATE(279)] = 6070,
  [SMALL_STATE(280)] = 6074,
  [SMALL_STATE(281)] = 6078,
  [SMALL_STATE(282)] = 6082,
  [SMALL_STATE(283)] = 6086,
  [SMALL_STATE(284)] = 6090,
  [SMALL_STATE(285)] = 6094,
  [SMALL_STATE(286)] = 6098,
  [SMALL_STATE(287)] = 6102,
  [SMALL_STATE(288)] = 6106,
  [SMALL_STATE(289)] = 6110,
  [SMALL_STATE(290)] = 6114,
  [SMALL_STATE(291)] = 6118,
  [SMALL_STATE(292)] = 6122,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 0),
  [5] = {.entry = {.count = 1, .reusable = true}}, SHIFT(268),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT(190),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(291),
  [11] = {.entry = {.count = 1, .reusable = true}}, SHIFT(289),
  [13] = {.entry = {.count = 1, .reusable = true}}, SHIFT(285),
  [15] = {.entry = {.count = 1, .reusable = true}}, SHIFT(283),
  [17] = {.entry = {.count = 1, .reusable = true}}, SHIFT(278),
  [19] = {.entry = {.count = 1, .reusable = true}}, SHIFT(272),
  [21] = {.entry = {.count = 1, .reusable = true}}, SHIFT(87),
  [23] = {.entry = {.count = 1, .reusable = true}}, SHIFT(30),
  [25] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [27] = {.entry = {.count = 1, .reusable = false}}, SHIFT(277),
  [29] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [31] = {.entry = {.count = 1, .reusable = false}}, SHIFT(276),
  [33] = {.entry = {.count = 1, .reusable = false}}, SHIFT(273),
  [35] = {.entry = {.count = 1, .reusable = false}}, SHIFT(45),
  [37] = {.entry = {.count = 1, .reusable = false}}, SHIFT(251),
  [39] = {.entry = {.count = 1, .reusable = true}}, SHIFT(51),
  [41] = {.entry = {.count = 1, .reusable = false}}, SHIFT(75),
  [43] = {.entry = {.count = 1, .reusable = true}}, SHIFT(71),
  [45] = {.entry = {.count = 1, .reusable = false}}, SHIFT(84),
  [47] = {.entry = {.count = 1, .reusable = true}}, SHIFT(62),
  [49] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [51] = {.entry = {.count = 1, .reusable = true}}, SHIFT(2),
  [53] = {.entry = {.count = 1, .reusable = true}}, SHIFT(23),
  [55] = {.entry = {.count = 1, .reusable = true}}, SHIFT(24),
  [57] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [59] = {.entry = {.count = 1, .reusable = true}}, SHIFT(17),
  [61] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [63] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [65] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_gamma_expr, 5),
  [67] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_gamma_expr, 5),
  [69] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 3),
  [71] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 3),
  [73] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 2),
  [75] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 2),
  [77] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 5),
  [79] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 5),
  [81] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 4),
  [83] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 4),
  [85] = {.entry = {.count = 1, .reusable = true}}, SHIFT(77),
  [87] = {.entry = {.count = 1, .reusable = false}}, SHIFT(49),
  [89] = {.entry = {.count = 1, .reusable = false}}, SHIFT(59),
  [91] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_gamma_expr, 3),
  [93] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_gamma_expr, 3),
  [95] = {.entry = {.count = 1, .reusable = false}}, SHIFT(219),
  [97] = {.entry = {.count = 1, .reusable = true}}, SHIFT(80),
  [99] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_stmt, 1),
  [101] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_expr, 1),
  [103] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_stmt, 1),
  [105] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_expr, 1),
  [107] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(30),
  [110] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(31),
  [113] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(49),
  [116] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(251),
  [119] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(51),
  [122] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(75),
  [125] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(71),
  [128] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_list_repeat1, 2), SHIFT_REPEAT(59),
  [131] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(30),
  [134] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(31),
  [137] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(49),
  [140] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(251),
  [143] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(51),
  [146] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(75),
  [149] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(71),
  [152] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 14), SHIFT_REPEAT(59),
  [155] = {.entry = {.count = 1, .reusable = true}}, SHIFT(54),
  [157] = {.entry = {.count = 1, .reusable = true}}, SHIFT(36),
  [159] = {.entry = {.count = 1, .reusable = true}}, SHIFT(108),
  [161] = {.entry = {.count = 1, .reusable = false}}, SHIFT(88),
  [163] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_scope_call_repeat1, 2),
  [165] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_scope_call_repeat1, 2),
  [167] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_scope_call_repeat1, 2), SHIFT_REPEAT(3),
  [170] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_scope_call, 2, .production_id = 8),
  [172] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_scope_call, 2, .production_id = 8),
  [174] = {.entry = {.count = 1, .reusable = true}}, SHIFT(3),
  [176] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [178] = {.entry = {.count = 1, .reusable = true}}, SHIFT(176),
  [180] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_float_literal, 2),
  [182] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_float_literal, 2),
  [184] = {.entry = {.count = 1, .reusable = true}}, SHIFT(73),
  [186] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2),
  [188] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(277),
  [191] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(276),
  [194] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(273),
  [197] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(45),
  [200] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2),
  [202] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(226),
  [205] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat1, 2), SHIFT_REPEAT(62),
  [208] = {.entry = {.count = 1, .reusable = true}}, SHIFT(202),
  [210] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_expr, 3),
  [212] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_expr, 3),
  [214] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_binary_expr, 3),
  [216] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_binary_expr, 3),
  [218] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_fn_call, 4, .production_id = 12),
  [220] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_fn_call, 4, .production_id = 12),
  [222] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_eval_expr, 7, .production_id = 28),
  [224] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_eval_expr, 7, .production_id = 28),
  [226] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_unary_expr, 2),
  [228] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_unary_expr, 2),
  [230] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_list, 4),
  [232] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_list, 4),
  [234] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_int_literal, 1),
  [236] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_int_literal, 1),
  [238] = {.entry = {.count = 1, .reusable = true}}, SHIFT(61),
  [240] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_field_access, 3, .production_id = 16),
  [242] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_field_access, 3, .production_id = 16),
  [244] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_field_access_repeat1, 2, .production_id = 15),
  [246] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_float_literal, 3),
  [248] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_float_literal, 3),
  [250] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_tuple, 4),
  [252] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_tuple, 4),
  [254] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_bool_literal, 1),
  [256] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_bool_literal, 1),
  [258] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_field_access, 4, .production_id = 25),
  [260] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_field_access, 4, .production_id = 25),
  [262] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_fn_call, 3, .production_id = 8),
  [264] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_fn_call, 3, .production_id = 8),
  [266] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_list, 3),
  [268] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_list, 3),
  [270] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_splat_expr, 5),
  [272] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_splat_expr, 5),
  [274] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_eval_expr, 6),
  [276] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_eval_expr, 6),
  [278] = {.entry = {.count = 1, .reusable = false}}, SHIFT(56),
  [280] = {.entry = {.count = 1, .reusable = true}}, SHIFT(37),
  [282] = {.entry = {.count = 1, .reusable = false}}, SHIFT(50),
  [284] = {.entry = {.count = 1, .reusable = true}}, SHIFT(50),
  [286] = {.entry = {.count = 1, .reusable = true}}, SHIFT(56),
  [288] = {.entry = {.count = 1, .reusable = true}}, SHIFT(52),
  [290] = {.entry = {.count = 1, .reusable = false}}, SHIFT(39),
  [292] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [294] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(268),
  [297] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(190),
  [300] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(291),
  [303] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(289),
  [306] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(285),
  [309] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(283),
  [312] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(278),
  [315] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(272),
  [318] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(86),
  [321] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1),
  [323] = {.entry = {.count = 1, .reusable = true}}, SHIFT(86),
  [325] = {.entry = {.count = 1, .reusable = false}}, SHIFT(170),
  [327] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
  [329] = {.entry = {.count = 1, .reusable = true}}, SHIFT(32),
  [331] = {.entry = {.count = 1, .reusable = true}}, SHIFT(187),
  [333] = {.entry = {.count = 1, .reusable = true}}, SHIFT(135),
  [335] = {.entry = {.count = 1, .reusable = true}}, SHIFT(168),
  [337] = {.entry = {.count = 1, .reusable = true}}, SHIFT(158),
  [339] = {.entry = {.count = 1, .reusable = true}}, SHIFT(237),
  [341] = {.entry = {.count = 1, .reusable = true}}, SHIFT(263),
  [343] = {.entry = {.count = 1, .reusable = true}}, SHIFT(290),
  [345] = {.entry = {.count = 1, .reusable = true}}, SHIFT(159),
  [347] = {.entry = {.count = 1, .reusable = false}}, SHIFT(159),
  [349] = {.entry = {.count = 1, .reusable = true}}, SHIFT(22),
  [351] = {.entry = {.count = 1, .reusable = true}}, SHIFT(184),
  [353] = {.entry = {.count = 1, .reusable = true}}, SHIFT(180),
  [355] = {.entry = {.count = 1, .reusable = true}}, SHIFT(16),
  [357] = {.entry = {.count = 1, .reusable = true}}, SHIFT(179),
  [359] = {.entry = {.count = 1, .reusable = true}}, SHIFT(177),
  [361] = {.entry = {.count = 1, .reusable = true}}, SHIFT(78),
  [363] = {.entry = {.count = 1, .reusable = true}}, SHIFT(217),
  [365] = {.entry = {.count = 1, .reusable = true}}, SHIFT(150),
  [367] = {.entry = {.count = 1, .reusable = true}}, SHIFT(189),
  [369] = {.entry = {.count = 1, .reusable = true}}, SHIFT(74),
  [371] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_field_access_repeat1, 2, .production_id = 15),
  [373] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_stmt, 2),
  [375] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_stmt, 2),
  [377] = {.entry = {.count = 1, .reusable = false}}, SHIFT(61),
  [379] = {.entry = {.count = 1, .reusable = true}}, SHIFT(64),
  [381] = {.entry = {.count = 1, .reusable = false}}, SHIFT(38),
  [383] = {.entry = {.count = 1, .reusable = true}}, SHIFT(41),
  [385] = {.entry = {.count = 1, .reusable = true}}, SHIFT(42),
  [387] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [389] = {.entry = {.count = 1, .reusable = true}}, SHIFT(43),
  [391] = {.entry = {.count = 1, .reusable = true}}, SHIFT(33),
  [393] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__param_list, 1, .production_id = 9),
  [395] = {.entry = {.count = 1, .reusable = true}}, SHIFT(139),
  [397] = {.entry = {.count = 1, .reusable = true}}, SHIFT(70),
  [399] = {.entry = {.count = 1, .reusable = true}}, SHIFT(7),
  [401] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [403] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_theta_expr, 7),
  [405] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_theta_expr, 7),
  [407] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__param_list, 2, .production_id = 13),
  [409] = {.entry = {.count = 1, .reusable = true}}, SHIFT(55),
  [411] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_assign, 3),
  [413] = {.entry = {.count = 1, .reusable = false}}, SHIFT(46),
  [415] = {.entry = {.count = 1, .reusable = true}}, SHIFT(44),
  [417] = {.entry = {.count = 1, .reusable = true}}, SHIFT(53),
  [419] = {.entry = {.count = 1, .reusable = true}}, SHIFT(46),
  [421] = {.entry = {.count = 1, .reusable = true}}, SHIFT(34),
  [423] = {.entry = {.count = 1, .reusable = true}}, SHIFT(35),
  [425] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_let, 4),
  [427] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_csg, 4),
  [429] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_impl_block, 11, .production_id = 27),
  [431] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_def_operation, 5, .production_id = 11),
  [433] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_fn, 9, .production_id = 26),
  [435] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 9),
  [437] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym__param_list_repeat1, 2, .production_id = 9),
  [439] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_impl_block, 10, .production_id = 27),
  [441] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_def_entity, 6, .production_id = 10),
  [443] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_ct_attrib, 4),
  [445] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_module, 3, .production_id = 2),
  [447] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_def_entity, 5, .production_id = 10),
  [449] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_fn, 7, .production_id = 24),
  [451] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_fn, 8, .production_id = 24),
  [453] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_impl_block, 7, .production_id = 23),
  [455] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_def_operation, 6, .production_id = 11),
  [457] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_fn, 8, .production_id = 26),
  [459] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_list_repeat1, 2),
  [461] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_list_repeat1, 2),
  [463] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_impl_block, 8, .production_id = 23),
  [465] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_def_concept, 7, .production_id = 21),
  [467] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_shape, 2),
  [469] = {.entry = {.count = 1, .reusable = true}}, SHIFT(292),
  [471] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_shape, 4),
  [473] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_shape, 5),
  [475] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__shaped_type, 1, .production_id = 5),
  [477] = {.entry = {.count = 1, .reusable = true}}, SHIFT(153),
  [479] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_shape, 1),
  [481] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_data_type, 1),
  [483] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__shaped_type, 4, .production_id = 22),
  [485] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_type, 4, .production_id = 19),
  [487] = {.entry = {.count = 1, .reusable = true}}, SHIFT(235),
  [489] = {.entry = {.count = 1, .reusable = true}}, SHIFT(236),
  [491] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__shaped_type, 1, .production_id = 7),
  [493] = {.entry = {.count = 1, .reusable = true}}, SHIFT(253),
  [495] = {.entry = {.count = 1, .reusable = true}}, SHIFT(242),
  [497] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_type, 1, .production_id = 6),
  [499] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_type, 1),
  [501] = {.entry = {.count = 1, .reusable = true}}, SHIFT(275),
  [503] = {.entry = {.count = 1, .reusable = true}}, SHIFT(114),
  [505] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_type_repeat1, 2, .production_id = 20),
  [507] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_type_repeat1, 2, .production_id = 20), SHIFT_REPEAT(134),
  [510] = {.entry = {.count = 1, .reusable = true}}, SHIFT(215),
  [512] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_shape_repeat1, 2),
  [514] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_shape_repeat1, 2), SHIFT_REPEAT(265),
  [517] = {.entry = {.count = 1, .reusable = true}}, SHIFT(156),
  [519] = {.entry = {.count = 1, .reusable = true}}, SHIFT(265),
  [521] = {.entry = {.count = 1, .reusable = true}}, SHIFT(72),
  [523] = {.entry = {.count = 1, .reusable = true}}, SHIFT(178),
  [525] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_block_repeat2, 2),
  [527] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_block_repeat2, 2), SHIFT_REPEAT(178),
  [530] = {.entry = {.count = 1, .reusable = true}}, SHIFT(15),
  [532] = {.entry = {.count = 1, .reusable = true}}, SHIFT(76),
  [534] = {.entry = {.count = 1, .reusable = true}}, SHIFT(106),
  [536] = {.entry = {.count = 1, .reusable = true}}, SHIFT(155),
  [538] = {.entry = {.count = 1, .reusable = true}}, SHIFT(21),
  [540] = {.entry = {.count = 1, .reusable = true}}, SHIFT(233),
  [542] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__arg_list_repeat1, 2), SHIFT_REPEAT(236),
  [545] = {.entry = {.count = 1, .reusable = true}}, SHIFT(11),
  [547] = {.entry = {.count = 1, .reusable = true}}, SHIFT(162),
  [549] = {.entry = {.count = 1, .reusable = true}}, SHIFT(134),
  [551] = {.entry = {.count = 1, .reusable = true}}, SHIFT(213),
  [553] = {.entry = {.count = 1, .reusable = true}}, SHIFT(200),
  [555] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__arg_list, 2),
  [557] = {.entry = {.count = 1, .reusable = true}}, SHIFT(241),
  [559] = {.entry = {.count = 1, .reusable = true}}, SHIFT(173),
  [561] = {.entry = {.count = 1, .reusable = true}}, SHIFT(232),
  [563] = {.entry = {.count = 1, .reusable = true}}, SHIFT(220),
  [565] = {.entry = {.count = 1, .reusable = true}}, SHIFT(281),
  [567] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__path, 2, .production_id = 3),
  [569] = {.entry = {.count = 1, .reusable = true}}, SHIFT(245),
  [571] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__ident_list_repeat1, 2), SHIFT_REPEAT(282),
  [574] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym__path_repeat1, 2, .production_id = 4), SHIFT_REPEAT(234),
  [577] = {.entry = {.count = 1, .reusable = true}}, SHIFT(181),
  [579] = {.entry = {.count = 1, .reusable = true}}, SHIFT(221),
  [581] = {.entry = {.count = 1, .reusable = true}}, SHIFT(225),
  [583] = {.entry = {.count = 1, .reusable = true}}, SHIFT(211),
  [585] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_type_repeat1, 2, .production_id = 18),
  [587] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__arg_list, 1),
  [589] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__ident_list, 2),
  [591] = {.entry = {.count = 1, .reusable = true}}, SHIFT(229),
  [593] = {.entry = {.count = 1, .reusable = true}}, SHIFT(182),
  [595] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__path, 1),
  [597] = {.entry = {.count = 1, .reusable = true}}, SHIFT(254),
  [599] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__ident_list, 1),
  [601] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_field_access_repeat1, 2, .production_id = 17), SHIFT_REPEAT(185),
  [604] = {.entry = {.count = 1, .reusable = true}}, SHIFT(259),
  [606] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_typed_arg, 3),
  [608] = {.entry = {.count = 1, .reusable = true}}, SHIFT(166),
  [610] = {.entry = {.count = 1, .reusable = true}}, SHIFT(286),
  [612] = {.entry = {.count = 1, .reusable = true}}, SHIFT(199),
  [614] = {.entry = {.count = 1, .reusable = true}}, SHIFT(39),
  [616] = {.entry = {.count = 1, .reusable = true}}, SHIFT(148),
  [618] = {.entry = {.count = 1, .reusable = true}}, SHIFT(67),
  [620] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__ident_list_repeat1, 2),
  [622] = {.entry = {.count = 1, .reusable = true}}, SHIFT(183),
  [624] = {.entry = {.count = 1, .reusable = true}}, SHIFT(256),
  [626] = {.entry = {.count = 1, .reusable = true}}, SHIFT(144),
  [628] = {.entry = {.count = 1, .reusable = true}}, SHIFT(91),
  [630] = {.entry = {.count = 1, .reusable = true}}, SHIFT(160),
  [632] = {.entry = {.count = 1, .reusable = true}}, SHIFT(48),
  [634] = {.entry = {.count = 1, .reusable = true}}, SHIFT(47),
  [636] = {.entry = {.count = 1, .reusable = true}}, SHIFT(40),
  [638] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__arg_list_repeat1, 2),
  [640] = {.entry = {.count = 1, .reusable = true}}, SHIFT(101),
  [642] = {.entry = {.count = 1, .reusable = true}}, SHIFT(19),
  [644] = {.entry = {.count = 1, .reusable = true}}, SHIFT(280),
  [646] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym__path_repeat1, 2, .production_id = 1),
  [648] = {.entry = {.count = 1, .reusable = true}}, SHIFT(142),
  [650] = {.entry = {.count = 1, .reusable = true}}, SHIFT(141),
  [652] = {.entry = {.count = 1, .reusable = true}}, SHIFT(93),
  [654] = {.entry = {.count = 1, .reusable = true}}, SHIFT(279),
  [656] = {.entry = {.count = 1, .reusable = true}}, SHIFT(66),
  [658] = {.entry = {.count = 1, .reusable = true}}, SHIFT(249),
  [660] = {.entry = {.count = 1, .reusable = true}}, SHIFT(248),
  [662] = {.entry = {.count = 1, .reusable = true}}, SHIFT(103),
  [664] = {.entry = {.count = 1, .reusable = true}}, SHIFT(79),
  [666] = {.entry = {.count = 1, .reusable = true}}, SHIFT(198),
  [668] = {.entry = {.count = 1, .reusable = true}}, SHIFT(165),
  [670] = {.entry = {.count = 1, .reusable = true}}, SHIFT(223),
  [672] = {.entry = {.count = 1, .reusable = true}}, SHIFT(169),
  [674] = {.entry = {.count = 1, .reusable = true}}, SHIFT(96),
  [676] = {.entry = {.count = 1, .reusable = true}}, SHIFT(163),
  [678] = {.entry = {.count = 1, .reusable = true}}, SHIFT(154),
  [680] = {.entry = {.count = 1, .reusable = true}}, SHIFT(206),
  [682] = {.entry = {.count = 1, .reusable = true}}, SHIFT(203),
  [684] = {.entry = {.count = 1, .reusable = true}}, SHIFT(247),
  [686] = {.entry = {.count = 1, .reusable = true}}, SHIFT(143),
  [688] = {.entry = {.count = 1, .reusable = true}}, SHIFT(214),
  [690] = {.entry = {.count = 1, .reusable = true}}, SHIFT(107),
  [692] = {.entry = {.count = 1, .reusable = true}}, SHIFT(224),
  [694] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [696] = {.entry = {.count = 1, .reusable = true}}, SHIFT(257),
  [698] = {.entry = {.count = 1, .reusable = true}}, SHIFT(238),
  [700] = {.entry = {.count = 1, .reusable = true}}, SHIFT(227),
  [702] = {.entry = {.count = 1, .reusable = true}}, SHIFT(137),
  [704] = {.entry = {.count = 1, .reusable = true}}, SHIFT(239),
  [706] = {.entry = {.count = 1, .reusable = true}}, SHIFT(240),
  [708] = {.entry = {.count = 1, .reusable = true}}, SHIFT(258),
  [710] = {.entry = {.count = 1, .reusable = true}}, SHIFT(243),
  [712] = {.entry = {.count = 1, .reusable = true}}, SHIFT(98),
  [714] = {.entry = {.count = 1, .reusable = true}}, SHIFT(196),
  [716] = {.entry = {.count = 1, .reusable = true}}, SHIFT(194),
  [718] = {.entry = {.count = 1, .reusable = true}}, SHIFT(99),
  [720] = {.entry = {.count = 1, .reusable = true}}, SHIFT(260),
  [722] = {.entry = {.count = 1, .reusable = true}}, SHIFT(264),
  [724] = {.entry = {.count = 1, .reusable = true}}, SHIFT(161),
  [726] = {.entry = {.count = 1, .reusable = true}}, SHIFT(152),
  [728] = {.entry = {.count = 1, .reusable = true}}, SHIFT(261),
  [730] = {.entry = {.count = 1, .reusable = true}}, SHIFT(231),
  [732] = {.entry = {.count = 1, .reusable = true}}, SHIFT(262),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_vola(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
