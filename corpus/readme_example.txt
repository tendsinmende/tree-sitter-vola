==================
README def block
==================


#[derive(LocalLipshitz)]
entity Sphere(radius: vec3);

concept SDF2D: vec2 -> real;
concept SDF3D: vec3 -> real;
concept UGF: vec3 -> real;
concept LocalLipshitz: vec3 -> real;
concept Color: none -> vec3;

---
(source_file
      (ct_attrib
        (fn_call
          (identifier)
          (expr
            (identifier))))
      (def_entity
        (identifier)
        (typed_arg
          (identifier)
          (type
            (shape
              (digit)))))
      (def_concept
        (identifier)
        (type
          (shape
            (digit)))
        (type
          (data_type)))
      (def_concept
        (identifier)
        (type
          (shape
            (digit)))
        (type
          (data_type)))
      (def_concept
        (identifier)
        (type
          (shape
            (digit)))
        (type
          (data_type)))
      (def_concept
        (identifier)
        (type
          (shape
            (digit)))
        (type
          (data_type)))
      (def_concept
        (identifier)
        (type
          (data_type))
        (type
          (shape
            (digit)))))

==================
README Sphere
==================
//Declares that we need the parameter `@` to define SDF3D of Sphere
impl Sphere for SDF3D(at){
	length(at) - radius
}

impl Sphere for UGF(at){
     length(at)
}

---
(source_file
      (comment)
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (block
          (expr
            (binary_expr
              (expr
                (fn_call
                  (identifier)
                  (expr
                    (identifier))))
              (expr
                (identifier))))))
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (block
          (expr
            (fn_call
              (identifier)
              (expr
                (identifier)))))))

==================
README op Translate
==================

#[define(no_identity)]
operation Translate(trans: vec3);

//declares that `translate` takes a sub-expression and the
//field
impl Translate<sub> for SDF3D(at){
  at = at - trans;
  eval sub.SDF3D(at)
}

---
(source_file
      (ct_attrib
        (fn_call
          (identifier)
          (expr
            (identifier))))
      (def_operation
        (identifier)
        (typed_arg
          (identifier)
          (type
            (shape
              (digit)))))
      (comment)
      (comment)
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (identifier)
        (block
          (stmt
            (assign
              (identifier)
              (expr
                (binary_expr
                  (expr
                    (identifier))
                  (expr
                    (identifier))))))
          (expr
            (eval_expr
              (identifier)
              (identifier)
              (expr
                (identifier)))))))

==================
README Op Union
==================

operation Union();
impl Union<left, right> for SDF3D(at) {
  let left = eval l.SDF3D(at);
  let right = eval r.SDF3D(at);
  let new_result = min(left, right);
  new_result
}

---
(source_file
      (def_operation
        (identifier))
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (eval_expr
                  (identifier)
                  (identifier)
                  (expr
                    (identifier))))))
          (stmt
            (let
              (identifier)
              (expr
                (eval_expr
                  (identifier)
                  (identifier)
                  (expr
                    (identifier))))))
          (stmt
            (let
              (identifier)
              (expr
                (fn_call
                  (identifier)
                  (expr
                    (identifier))
                  (expr
                    (identifier))))))
          (expr
            (identifier)))))

==================
README Op SmoothUnion
==================

operation SmoothUnion(suradius: real);
impl SmoothUnion<left, right> for SDF3D(at){
  let l = eval left.SDF3D(at);
  let r = eval right.SDF3D(at);
  let h = clamp( 0.5 + 0.5 * (r-l) / suradius, 0.0, 1.0 );
  let dist = mix(l, r, h) * - suradius * h * (1.0 - h);
  dist
}

---
(source_file
      (def_operation
        (identifier)
        (typed_arg
          (identifier)
          (type
            (data_type))))
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (eval_expr
                  (identifier)
                  (identifier)
                  (expr
                    (identifier))))))
          (stmt
            (let
              (identifier)
              (expr
                (eval_expr
                  (identifier)
                  (identifier)
                  (expr
                    (identifier))))))
          (stmt
            (let
              (identifier)
              (expr
                (fn_call
                  (identifier)
                  (expr
                    (binary_expr
                      (expr
                        (float_literal
                          (digit)
                          (digit)))
                      (expr
                        (binary_expr
                          (expr
                            (binary_expr
                              (expr
                                (float_literal
                                  (digit)
                                  (digit)))
                              (expr
                                (expr
                                  (binary_expr
                                    (expr
                                      (identifier))
                                    (expr
                                      (identifier)))))))
                          (expr
                            (identifier))))))
                  (expr
                    (float_literal
                      (digit)
                      (digit)))
                  (expr
                    (float_literal
                      (digit)
                      (digit)))))))
          (stmt
            (let
              (identifier)
              (expr
                (binary_expr
                  (expr
                    (binary_expr
                      (expr
                        (binary_expr
                          (expr
                            (fn_call
                              (identifier)
                              (expr
                                (identifier))
                              (expr
                                (identifier))
                              (expr
                                (identifier))))
                          (expr
                            (unary_expr
                              (expr
                                (identifier))))))
                      (expr
                        (identifier))))
                  (expr
                    (expr
                      (binary_expr
                        (expr
                          (float_literal
                            (digit)
                            (digit)))
                        (expr
                          (identifier)))))))))
          (expr
            (identifier)))))

==================
README Op ColorOW
==================

operation ColorOw(owcol: vec3);
impl ColorOw<sub> for Color(){
  owcol
}

---
(source_file
      (def_operation
        (identifier)
        (typed_arg
          (identifier)
          (type
            (shape
              (digit)))))
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (block
          (expr
            (identifier)))))

==================
README subField define
==================

fn subField(trans: vec3) -> csg{
    csg sphere = Sphere(3.0);

    Union(){
        Translate(trans){
            Sphere(2.0)
        }
    }{
        Box([1.0, 0.0, 3.0])
    }
}

---
(source_file
      (fn
        (identifier)
        (typed_arg
          (identifier)
          (type
            (shape
              (digit))))
        (type)
        (block
          (stmt
            (csg
              (identifier)
              (expr
                (fn_call
                  (identifier)
                  (expr
                    (float_literal
                      (digit)
                      (digit)))))))
          (expr
            (scope_call
              (fn_call
                (identifier))
              (block
                (expr
                  (scope_call
                    (fn_call
                      (identifier)
                      (expr
                        (identifier)))
                    (block
                      (expr
                        (fn_call
                          (identifier)
                          (expr
                            (float_literal
                              (digit)
                              (digit)))))))))
              (block
                (expr
                  (fn_call
                    (identifier)
                    (expr
                      (list
                        (expr
                          (float_literal
                            (digit)
                            (digit)))
                        (expr
                          (float_literal
                            (digit)
                            (digit)))
                        (expr
                          (float_literal
                            (digit)
                            (digit)))))))))))))

==================
README export myField
==================

//Export interface of the SDF
export fn myField(p: vec3, translation: vec3) -> (real, vec3){
    let some_formula = translation.x * 2.0;
    csg all_field = SmoothUnion(1.0){
        subField(translation)
    }{
        Rotate([30.0, 30.0, some_formula]){
            Round(1.0){
                Box([1.0, 0.5, 3.0])
            }
        }
    };

    (eval all_field.SDF3D(p), eval all_field.Color(p))
}

---
(source_file
      (comment)
      (fn
        (identifier)
        (typed_arg
          (identifier)
          (type
            (shape
              (digit))))
        (typed_arg
          (identifier)
          (type
            (shape
              (digit))))
        (type
          (data_type)
          (shape
            (digit)))
        (block
          (stmt
            (let
              (identifier)
              (expr
                (binary_expr
                  (expr
                    (field_access
                      (identifier)
                      (identifier)))
                  (expr
                    (float_literal
                      (digit)
                      (digit)))))))
          (stmt
            (csg
              (identifier)
              (expr
                (scope_call
                  (fn_call
                    (identifier)
                    (expr
                      (float_literal
                        (digit)
                        (digit))))
                  (block
                    (expr
                      (fn_call
                        (identifier)
                        (expr
                          (identifier)))))
                  (block
                    (expr
                      (scope_call
                        (fn_call
                          (identifier)
                          (expr
                            (list
                              (expr
                                (float_literal
                                  (digit)
                                  (digit)))
                              (expr
                                (float_literal
                                  (digit)
                                  (digit)))
                              (expr
                                (identifier)))))
                        (block
                          (expr
                            (scope_call
                              (fn_call
                                (identifier)
                                (expr
                                  (float_literal
                                    (digit)
                                    (digit))))
                              (block
                                (expr
                                  (fn_call
                                    (identifier)
                                    (expr
                                      (list
                                        (expr
                                          (float_literal
                                            (digit)
                                            (digit)))
                                        (expr
                                          (float_literal
                                            (digit)
                                            (digit)))
                                        (expr
                                          (float_literal
                                            (digit)
                                            (digit))))))))))))))))))
          (expr
            (tuple
              (expr
                (eval_expr
                  (identifier)
                  (identifier)
                  (expr
                    (identifier))))
              (expr
                (eval_expr
                  (identifier)
                  (identifier)
                  (expr
                    (identifier)))))))))
