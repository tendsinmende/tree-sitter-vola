==================
alge add
==================

impl Test for Test(){
    a + b
}

---
(source_file
      (impl_block
        (identifier)
        (identifier)
        (block
          (expr
            (binary_expr
              (expr
                (identifier))
              (expr
                (identifier)))))))

==================
alge let
==================

impl Test for Test(){
    let c = 3.0;
    c
}

---
(source_file
      (impl_block
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (float_literal
                  (digit)
                  (digit)))))
          (expr
            (identifier)))))

==================
alge left-associativity-binary
==================

impl Test for Test(){
    let c = 3.0;
    3.0 + 3 + c
}

---
(source_file
      (impl_block
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (float_literal
                  (digit)
                  (digit)))))
          (expr
            (binary_expr
              (expr
                (binary_expr
                  (expr
                    (float_literal
                      (digit)
                      (digit)))
                  (expr
                    (int_literal
                    (digit)))))
              (expr
                (identifier)))))))

==================
alge mul-before-additive
==================

impl Test for Test(){
    let c = 3.0;
    3.0 * 3 + c
}

---
(source_file
      (impl_block
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (float_literal
                  (digit)
                  (digit)))))
          (expr
            (binary_expr
              (expr
                (binary_expr
                  (expr
                    (float_literal
                      (digit)
                      (digit)))
                  (expr
                    (int_literal
                    (digit)))))
              (expr
                (identifier)))))))

==================
alge mul-before-additive reverse
==================

impl Test for Test(){
    let c = 3.0;
    3.0 + 3 * c
}

---
(source_file
      (impl_block
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (float_literal
                  (digit)
                  (digit)))))
          (expr
            (binary_expr
              (expr
                (float_literal
                  (digit)
                  (digit)))
              (expr
                (binary_expr
                  (expr
                    (int_literal
                    (digit)))
                  (expr
                    (identifier)))))))))

==================
alge braces
==================

impl Test for Test(){
    let c = 3.0;
    (3.0 + 3) * c
}

---
(source_file
      (impl_block
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (float_literal
                  (digit)
                  (digit)))))
          (expr
            (binary_expr
              (expr
                (expr
                  (binary_expr
                    (expr
                      (float_literal
                        (digit)
                        (digit)))
                    (expr
                      (int_literal
                      (digit))))))
              (expr
                (identifier)))))))

==================
alge operation unary
==================

fn test(a: real) -> real{
  -real
}
---
(source_file
      (fn
        (identifier)
        (typed_arg
          (identifier)
          (type
            (data_type)))
        (type
          (data_type))
        (block
          (expr
            (unary_expr
              (expr
                (identifier)))))))

==================
alge operation unary multiple
==================
fn test(a: real) -> real{
  ---a
}
---
(source_file
      (fn
        (identifier)
        (typed_arg
          (identifier)
          (type
            (data_type)))
        (type
          (data_type))
        (block
          (expr
            (unary_expr
              (expr
                (unary_expr
                  (expr
                    (unary_expr
                      (expr
                        (identifier)))))))))))

==================
alge operation unary/binary mixed
==================
fn test(a: real) -> real{
  --s + ---1.0
}
---
(source_file
      (fn
        (identifier)
        (typed_arg
          (identifier)
          (type
            (data_type)))
        (type
          (data_type))
        (block
          (expr
            (binary_expr
              (expr
                (unary_expr
                  (expr
                    (unary_expr
                      (expr
                        (identifier))))))
              (expr
                (unary_expr
                  (expr
                    (unary_expr
                      (expr
                        (unary_expr
                          (expr
                            (float_literal
                              (digit)
                              (digit))))))))))))))

==================
alge operation eval unary
==================

impl Test<sub> for Test(){
    let a = eval sub.Test();
    a
}

---

(source_file
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (eval_expr
                  (identifier)
                  (identifier)))))
          (expr
            (identifier)))))

==================
alge operation eval binary
==================

impl Test<sub1, sub2> for Test(){
    let left = eval sub1.Test();
    let right = eval sub2.Test();
    left + right
}

---
(source_file
      (impl_block
        (identifier)
        (identifier)
        (identifier)
        (identifier)
        (block
          (stmt
            (let
              (identifier)
              (expr
                (eval_expr
                  (identifier)
                  (identifier)))))
          (stmt
            (let
              (identifier)
              (expr
                (eval_expr
                  (identifier)
                  (identifier)))))
          (expr
            (binary_expr
              (expr
                (identifier))
              (expr
                (identifier)))))))

==================
gamma predicate expression simple nested
==================

fn test(a: real) -> real{
  if (a % 2.0) < 1.0 {
    test
  }
}

---
(source_file
      (fn
        (identifier)
        (typed_arg
          (identifier)
          (type
            (data_type)))
        (type
          (data_type))
        (block
          (stmt
            (gamma_expr
              (expr
                (binary_expr
                  (expr
                    (expr
                      (binary_expr
                        (expr
                          (identifier))
                        (expr
                          (float_literal
                            (digit)
                            (digit))))))
                  (expr
                    (float_literal
                      (digit)
                      (digit)))))
              (block
                (expr
                  (identifier))))))))

==================
gamma predicate expression with field access
==================

fn test(a: vec3) -> real{
  if (a.x % 2.0) < 1.0 {
    test
  }else{
    jaddah
  }
}

---
(source_file
      (fn
        (identifier)
        (typed_arg
          (identifier)
          (type
            (shape
              (digit))))
        (type
          (data_type))
        (block
          (stmt
            (gamma_expr
              (expr
                (binary_expr
                  (expr
                    (expr
                      (binary_expr
                        (expr
                          (field_access
                            (identifier)
                            (identifier)))
                        (expr
                          (float_literal
                            (digit)
                            (digit))))))
                  (expr
                    (float_literal
                      (digit)
                      (digit)))))
              (block
                (expr
                  (identifier)))
              (block
                (expr
                  (identifier))))))))
