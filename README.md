<div align="center">

# TreeSitte Vola

**VO**lume **LA**nguage: A research [DSL](https://en.wikipedia.org/wiki/Domain-specific_language) that evaluates volume functions.

Examples of such functions are [Signed Distance Functions](https://en.wikipedia.org/wiki/Signed_distance_function) or [Unit Gradient Fields](https://www.blakecourter.com/2023/05/18/field-notation.html).
The implementation will focus on SDFs at first.

[![dependency status](https://deps.rs/repo/gitlab/tendsinmende/vola/status.svg)](https://deps.rs/repo/gitlab/tendsinmende/vola)
[![MIT](https://img.shields.io/badge/License-MIT-blue)](LICENSE-MIT)
[![APACHE](https://img.shields.io/badge/License-Apache_2.0-blue)](LICENSE-APACHE)

</div>

The [Treesitter](https://github.com/tree-sitter/tree-sitter) implementation of [Vola's]() syntax.

Includes the generated parser.


## Building

### non-cargo dependencies

- [Trees-Sitter CLI](https://crates.io/crates/tree-sitter-cli)
- Node.js
- A C Compiler

## Building

Run this whenever changing the grammar.js file.
``` shell
tree-sitter generate
```

## Testing
Run this whenever changing the grammar.js file which tests it against all files in `corpus/`.
``` shell
tree-sitter test
```

## Parsing

To Parse a file, and output the AST:

```shell
tree-sitter parse /path/to/file.vola
```

## Syntax highlighting

```shell
tree-sitter highlight /path/to/file.vola
```
Note that you might have to point your tree-sitter [user-config](https://tree-sitter.github.io/tree-sitter/syntax-highlighting#per-user-configuration) to the repository location.

Usually this is done by appending `/path/to/tree-sitter-vola` to the `parser-directories` entry in `~/.config/tree-sitter/config.json`.
Ultimately, Vola should show up when running `tree-sitter dump-languages`.

## License

Licensed under either of

- Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or <http://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
