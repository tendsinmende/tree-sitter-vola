[ "for" "in" "if" "else" "let" "impl" "fn" "export" "csg" "eval"] @keyword
[ "module" ] @module
[ ";" ] @punctuation.delimiter

(def_concept concept_name: (identifier) @function)
(def_entity entity_name: (identifier) @function)
(def_operation operation_name: (identifier) @function)

(impl_block op_or_entity: (identifier) @function)
(impl_block concept: (identifier) @function)

["[" "]" "<" ">" ]  @punctuation.bracket
[ "*" "+" "-" "/" "=" "%" "==" "!=" "<" ">" "<=" ">=" "&&" "||" ] @operator
(fn_call function_name: (identifier) @function)


(comment) @comment
(type) @type
(float_literal) @constant
(bool_literal) @constant
(int_literal (digit) @constant)
