//Shamelessly taken from the RUST precedence rules. But we don't use all
const PREC = {
  call: 15,
  field: 14,
  try: 13,
  eval: 13,
  unary: 12,
  cast: 11,
  multiplicative: 10,
  additive: 9,
  shift: 8,
  bitand: 7,
  bitxor: 6,
  bitor: 5,
  comparative: 4,
  and: 3,
  or: 2,
  range: 1,
  assign: 0,
  closure: -1,
};

module.exports = grammar({
  name: "vola",
  rules: {
    source_file: ($) =>
      repeat(
        choice(
          $.comment,
          $.def_concept,
          $.def_entity,
          $.def_operation,
          $.impl_block,
          $.ct_attrib,
          $.fn,
          $.module,
        ),
      ),

    //the #[some(stuff)] syntax
    ct_attrib: ($) => seq("#", "[", $.fn_call, "]"),

    //the module / include syntax

    module: ($) => seq("module", $._path, ";"),

    _path: ($) =>
      seq(repeat(seq($.identifier, field("path_spacer", "::"))), $.identifier),

    //=============================================

    // entity // concept // operation

    def_entity: ($) =>
      seq(
        "entity",
        field("entity_name", $.identifier),
        "(",
        optional($._arg_list),
        ")",
        ";",
      ),

    def_concept: ($) =>
      seq(
        "concept",
        field("concept_name", $.identifier),
        ":",
        $.type,
        "->",
        field("concept_result", $.type),
        ";",
      ),

    def_operation: ($) =>
      seq(
        "operation",
        field("operation_name", $.identifier),
        "(",
        optional($._arg_list),
        ")",
        ";",
      ),

    //impl syntax

    impl_block: ($) =>
      seq(
        "impl",
        field("op_or_entity", $.identifier),
        optional(seq("<", field("operand", $._ident_list), ">")),
        "for",
        field("concept", $.identifier),
        seq("(", optional($.identifier), ")"),
        $.block,
      ),

    fn: ($) =>
      seq(
        optional("export"),
        "fn",
        field("fnname", $.identifier),
        "(",
        optional($._arg_list),
        ")",
        "->",
        $.type,
        $.block,
      ),

    _ty_list: ($) => seq(repeat(seq($.type, ",")), $.type),

    _arg_list: ($) => seq(repeat(seq($.typed_arg, ",")), $.typed_arg),

    typed_arg: ($) => seq($.identifier, ":", $.type),

    _ident_list: ($) => seq(repeat(seq($.identifier, ",")), $.identifier),

    //A _block_ is just a list of statements
    //in braces with possibly a return expression
    block: ($) =>
      seq(
        "{",
        repeat(choice($.stmt, $.comment)),
        optional(seq($.expr, repeat($.comment))),
        "}",
      ),

    let: ($) => seq("let", $.identifier, "=", $.expr),

    assign: ($) => seq($.identifier, "=", $.expr),

    csg: ($) => seq("csg", $.identifier, "=", $.expr),

    //A statment is just some expression-like thing
    //with a ; at the end or a gamma/theta expr
    stmt: ($) =>
      choice(
        prec.left(0, seq(choice($.let, $.assign, $.csg), ";")),
        $.theta_expr,
        prec.left(1, $.gamma_expr),
      ),

    //TODO: add sum and mul loops?
    expr: ($) =>
      prec.left(
        choice(
          seq("(", $.expr, ")"),
          $.unary_expr,
          $.binary_expr,
          $.eval_expr,
          $._literal,
          $.field_access,
          $.identifier,
          $.fn_call,
          $.scope_call,
          $.list,
          $.tuple,
          $.gamma_expr,
          $.splat_expr,
        ),
      ),

    //NOTE: right now we have just a simple if-then-else like syntax
    gamma_expr: ($) =>
      seq("if", $.expr, $.block, optional(seq("else", $.block))),

    //NOTE: currently we just allow simple _in range_ loops
    theta_expr: ($) =>
      seq("for", $.identifier, "in", $.expr, "..", $.expr, $.block),

    eval_expr: ($) =>
      prec.left(
        PREC.eval,
        seq(
          "eval",
          $.identifier,
          ".",
          $.identifier,
          "(",
          optional($._param_list),
          ")",
        ),
      ),

    unary_expr: ($) =>
      choice(
        prec.right(PREC.unary, seq("-", $.expr)),
        prec.right(PREC.unary, seq("!", $.expr)),
      ),

    field_access: ($) =>
      prec.left(
        PREC.field,
        seq(
          $.identifier,
          repeat(seq(field("access_splitter", "."), $._field_accessor)),
          ".",
          field("access_field", $._field_accessor),
        ),
      ),

    _field_accessor: ($) => choice($.identifier, $.digit),

    splat_expr: ($) => seq("[", $.expr, ";", $.int_literal, "]"),

    binary_expr: ($) =>
      choice(
        prec.left(PREC.multiplicative, seq($.expr, "/", $.expr)),
        prec.left(PREC.multiplicative, seq($.expr, "*", $.expr)),
        prec.left(PREC.additive, seq($.expr, "+", $.expr)),
        prec.left(PREC.additive, seq($.expr, "-", $.expr)),
        prec.left(PREC.additive, seq($.expr, "%", $.expr)),
        prec.left(PREC.comparative, seq($.expr, "==", $.expr)),
        prec.left(PREC.comparative, seq($.expr, "!=", $.expr)),
        prec.left(PREC.comparative, seq($.expr, "<", $.expr)),
        prec.left(PREC.comparative, seq($.expr, ">", $.expr)),
        prec.left(PREC.comparative, seq($.expr, "<=", $.expr)),
        prec.left(PREC.comparative, seq($.expr, ">=", $.expr)),
        prec.left(PREC.and, seq($.expr, "&&", $.expr)),
        prec.left(PREC.or, seq($.expr, "||", $.expr)),
      ),

    fn_call: ($) =>
      seq(
        field("function_name", $.identifier),
        "(",
        optional($._param_list),
        ")",
      ),

    scope_call: ($) =>
      prec.left(10, seq(field("function_name", $.fn_call), repeat1($.block))),

    _param_list: ($) =>
      seq(
        repeat(seq(field("parameter", $.expr), ",")),
        field("parameter", $.expr),
      ),

    type: ($) =>
      choice(
        //simple shaped type
        $._shaped_type,
        //a csg (tree)
        "csg",
        //tuple type of sorts. Always has at least 2 elements
        seq("(", $._shaped_type, repeat1(seq(",", $._shaped_type)), ")"),
      ),

    shape: ($) =>
      choice(
        "interval",
        seq("vec", $.digit),
        seq("mat", $.digit),
        seq("mat", $.digit, "x", $.digit),
        seq("tensor", "<", $.digit, repeat(seq(",", $.digit)), ">"),
      ),

    _shaped_type: ($) =>
      choice(
        //Atomic, unshaped
        field("dtype", $.data_type),
        //if only shape, without data-type, we default to reals
        field("shape", $.shape),
        //Otherwise it must be given
        seq(field("shape", $.shape), "<", field("dtype", $.data_type), ">"),
      ),

    data_type: ($) => choice("real", "int", "complex", "quat", "bool", "none"),

    //Common defs
    //=============================================

    list: ($) => seq("[", repeat(seq($.expr, ",")), $.expr, "]"),
    //Note: Tuple needs at least two elements
    tuple: ($) => seq("(", repeat1(seq($.expr, ",")), $.expr, ")"),

    _digit_list: ($) => seq(repeat(seq($.digit, ",")), $.digit),

    _literal: ($) => choice($.int_literal, $.float_literal, $.bool_literal),

    float_literal: ($) => seq($.digit, ".", optional($.digit)),

    bool_literal: ($) => choice("true", "false"),

    int_literal: ($) => $.digit,

    digit: ($) => /[0-9][0-9_]*/,
    //identifier: $ => /([a-zA-Z$][a-zA-Z_]*)/,
    identifier: (_) => /[_\p{XID_Start}][_\p{XID_Continue}]*/,
    //All about comments
    //=================

    comment: ($) => token(seq("//", /.*/)),
  },
});
